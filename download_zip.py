list_of_files = ["AD.zip","AE.zip","AF.zip","AG.zip","AI.zip","AL.zip","AM.zip","AN.zip","AO.zip","AQ.zip","AR.zip","AS.zip","AT.zip","AU.zip","AW.zip","AX.zip","AZ.zip","BA.zip","BB.zip","BD.zip","BE.zip","BF.zip","BG.zip","BH.zip","BI.zip","BJ.zip","BL.zip","BM.zip","BN.zip","BO.zip","BQ.zip","BR.zip","BS.zip","BT.zip","BV.zip","BW.zip","BY.zip","BZ.zip","CA.zip","CC.zip","CD.zip","CF.zip","CG.zip","CH.zip","CI.zip","CK.zip","CL.zip","CM.zip","CN.zip","CO.zip","CR.zip","CS.zip","CU.zip","CV.zip","CW.zip","CX.zip","CY.zip","CZ.zip","DE.zip","DJ.zip","DK.zip","DM.zip","DO.zip","DZ.zip","EC.zip","EE.zip","EG.zip","EH.zip","ER.zip","ES.zip","ET.zip","FI.zip","FJ.zip","FK.zip","FM.zip","FO.zip","FR.zip","GA.zip","GB.zip","GD.zip","GE.zip","GF.zip","GG.zip","GH.zip","GI.zip","GL.zip","GM.zip","GN.zip","GP.zip","GQ.zip","GR.zip","GS.zip","GT.zip","GU.zip","GW.zip","GY.zip","HK.zip","HM.zip","HN.zip","HR.zip","HT.zip","HU.zip","ID.zip","IE.zip","IL.zip","IM.zip","IN.zip","IO.zip","IQ.zip","IR.zip","IS.zip","IT.zip","JE.zip","JM.zip","JO.zip","JP.zip","KE.zip","KG.zip","KH.zip","KI.zip","KM.zip","KN.zip","KP.zip","KR.zip","KW.zip","KY.zip","KZ.zip","LA.zip","LB.zip","LC.zip","LI.zip","LK.zip","LR.zip","LS.zip","LT.zip","LU.zip","LV.zip","LY.zip","MA.zip","MC.zip","MD.zip","ME.zip","MF.zip","MG.zip","MH.zip","MK.zip","ML.zip","MM.zip","MN.zip","MO.zip","MP.zip","MQ.zip","MR.zip","MS.zip","MT.zip","MU.zip","MV.zip","MW.zip","MX.zip","MY.zip","MZ.zip","NA.zip","NC.zip","NE.zip","NF.zip","NG.zip","NI.zip","NL.zip","NO.zip","NP.zip","NR.zip","NU.zip","NZ.zip","OM.zip","PA.zip","PE.zip","PF.zip","PG.zip","PH.zip","PK.zip","PL.zip","PM.zip","PN.zip","PR.zip","PS.zip","PT.zip","PW.zip","PY.zip","QA.zip","RE.zip","RO.zip","RS.zip","RU.zip","RW.zip","SA.zip","SB.zip","SC.zip","SD.zip","SE.zip","SG.zip","SH.zip","SI.zip","SJ.zip","SK.zip","SL.zip","SM.zip","SN.zip","SO.zip","SR.zip","SS.zip","ST.zip","SV.zip","SX.zip","SY.zip","SZ.zip","TC.zip","TD.zip","TF.zip","TG.zip","TH.zip","TJ.zip","TK.zip","TL.zip","TM.zip","TN.zip","TO.zip","TR.zip","TT.zip","TV.zip","TW.zip","TZ.zip","UA.zip","UG.zip","UM.zip","US.zip","UY.zip","UZ.zip","VA.zip","VC.zip","VE.zip","VG.zip","VI.zip","VN.zip","VU.zip","WF.zip","WS.zip","XK.zip","YE.zip","YT.zip","YU.zip","ZA.zip","ZM.zip","ZW.zip"]
#
# import zipfile, urllib.request, shutil
# import os
# path = os.getcwd() + '/data/all_loc/'
# url = 'http://download.geonames.org/export/dump/'
# for file_name in list_of_files:
#     main_url = url+file_name
#     file_path = path + file_name
#     print(main_url)
#     try:
#         with urllib.request.urlopen(main_url) as response, open(file_path, 'wb') as out_file:
#             shutil.copyfileobj(response, out_file)
#             with zipfile.ZipFile(file_path) as zf:
#                 zf.extractall()
#     except Exception as e:
#         print(f"failed for --> {file_name}")
#         continue

#
# import zipfile
# import os
# path = os.getcwd() + '/data/all_loc/'
# for file_name in list_of_files:
#     zip_ref = zipfile.ZipFile(path+file_name, 'r')
#     zip_ref.extractall(path+file_name.replace(".zip",""))
#     zip_ref.close()

