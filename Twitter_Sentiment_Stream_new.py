import csv
import random
import json, re, threading
import pandas as pd
from datetime import datetime as dt
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from http.client import IncompleteRead
from tweepy import Stream
from senti_classifier import senti_classifier
from urllib3.exceptions import ProtocolError
import plotly.plotly as py
import plotly.graph_objs as go
import plotly

# plotly.tools.set_credentials_file(username='join2saurav', api_key='r8gIeW5ie1M7cTaLda4d')
# plotly.tools.set_credentials_file(username='deep123', api_key='cxAd4OWVogO4OKqX9Whe')
plotly.tools.set_credentials_file(username='argsss', api_key='czik5xbYiiocUjgGlYAo')
# nltk.download('punkt')
# nltk.download('wordnet')

#to_track = ["security, incident, delhi airport, mumbai airport"]
#location = [-122.75,36.8,-121.75,37.8,-74,40,-73,41]
#count = 2
# path = os.getcwd()
path = '/home/arghya/Documents/DataValAnalysis/'
tweet_csv = path+'/data/tweet_data.csv'

screen_nm_arr = ['53075131', '1844224638', '1373909136', '27212872', '221652567', '809641114026160129', '488251621',
                 '1582447969', '83805213', '1479352976', '541818082']
congress_pages = ['874957590127443968', '1418662837', '731711664257650692', '1053953509949755394', '1382548236',
                  '1660826017', '3928253712', '3257761386', '586328623', '1153045459', '719550364660293632']
cong_political_leaders = ['3066787711', '2892809359', '3171712086', '62101279', '2410755840', '24705126', '3097503906',
                          '2331591042', '19760270', '2938972718', '388724149']
bjp_political_leaders = ["95588504", "1368737382", "1447949844", "1346439824", "18839785", "2183816041",
                              "252018855", "130104041", "141208596", "97217966", "1324334436"]

to_track = ["rahul"]
to_follow = []
to_follow.extend(screen_nm_arr)
to_follow.extend(congress_pages)
to_follow.extend(cong_political_leaders)
to_follow.extend(bjp_political_leaders)

languages = ['en']

class TweetStreamListener(StreamListener):
    def __init__(self, count=0):
        self.counter = 0
        self.limit = count

    def compare_handlers(self, main_df):
        bjp_handlers = []
        bjp_handlers.extend(bjp_political_leaders)
        bjp_handlers.extend(screen_nm_arr)
        inc_handlers = []
        inc_handlers.extend(cong_political_leaders)
        inc_handlers.extend(congress_pages)

        tweet_data_bjp = main_df[main_df['original_user_id'].isin(bjp_handlers)]
        tweet_data_inc = main_df[main_df['original_user_id'].isin(inc_handlers)]
        tweet_data_bjp = tweet_data_bjp.groupby(['message', 'original_user_name']).size().reset_index(name='retweets_counts')
        tweet_data_inc = tweet_data_inc.groupby(['message', 'original_user_name']).size().reset_index(name='retweets_counts')
        tweet_plot_bjp = tweet_data_bjp[['original_user_name', 'message', 'retweets_counts']]
        tweet_plot_inc = tweet_data_inc[['original_user_name', 'message', 'retweets_counts']]

        table_trace1 = go.Table(
            domain=dict(x=[0, 1],
                        y=[0.7, 1.0]),
            columnwidth=[1, 2, 2, 2],
            columnorder=[0, 1, 2, 3, 4],
            header=dict(height=50,
                        values=['author_name', 'message', 'retweets_counts'],
                        line=dict(color='rgb(50, 50, 50)'),
                        align=['center'],
                        font=dict(color='#FFF2F2', size=18),
                        fill=dict(color='#511010')),
            cells=dict(values=[tweet_plot_inc[k].tolist() for k in ['original_user_name', 'message', 'retweets_counts']],
                       line=dict(color='#506784'),
                       align=['left'] * 5,
                       font=dict(color='#511010', size=12),
                       format=[None] + [", .2f"] * 2 + [',.4f'],
                       prefix=[None] * 2 + ['$', u'\u20BF'],
                       suffix=[None] * 4,
                       height=27,
                       fill=dict(color='#FFF2F2'))
        )

        table_trace2 = go.Table(
            domain=dict(x=[0, 1],
                        y=[0.7, 1.0]),
            columnwidth=[1, 2, 2, 2],
            columnorder=[0, 1, 2, 3, 4],
            header=dict(height=50,
                        values=['author_name', 'message', 'retweets_counts'],
                        line=dict(color='rgb(50, 50, 50)'),
                        align=['center'],
                        font=dict(color='#FFF2F2', size=18),
                        fill=dict(color='#511010')),
            cells=dict(values=[tweet_plot_bjp[k].tolist() for k in ['original_user_name', 'message', 'retweets_counts']],
                       line=dict(color='#506784'),
                       align=['left'] * 5,
                       font=dict(color='#511010', size=12),
                       format=[None] + [", .2f"] * 2 + [',.4f'],
                       prefix=[None] * 2 + ['$', u'\u20BF'],
                       suffix=[None] * 4,
                       height=27,
                       fill=dict(color='#FFF2F2'))
        )

        axis = dict(
            showline=True,
            zeroline=False,
            showgrid=True,
            mirror=True,
            ticklen=4,
            gridcolor='#ffffff',
            tickfont=dict(size=10)
        )

        layout2 = dict(
            width=950,
            height=800,
            autosize=False,
            title='Bitcoin mining stats for 180 days',
            margin=dict(t=100),
            showlegend=False,
            xaxis1=dict(axis, **dict(domain=[0, 1], anchor='y1', showticklabels=False)),
            xaxis2=dict(axis, **dict(domain=[0, 1], anchor='y2', showticklabels=False)),
            yaxis1=dict(axis, **dict(domain=[2 * 0.21 + 0.02 + 0.02, 0.68], anchor='x1', hoverformat='.2f')),
            yaxis2=dict(axis,
                        **dict(domain=[0.21 + 0.02, 2 * 0.21 + 0.02], anchor='x2', tickprefix='$', hoverformat='.2f')),
            plot_bgcolor='rgba(228, 222, 249, 0.65)'
        )

        fig2 = dict(data=[table_trace1, table_trace2], layout=layout2)
        py.iplot(fig2, filename='vertical-stacked-subplot-tables')


    def on_data(self, data):
        temp_dict1, temp_dict2 = {}, {}
        if self.limit is not 0:
            self.counter +=1
            if self.counter > self.limit:
                return False
        dict_data = json.loads(data)
        
        #print(data)
        try:
            text = dict_data["text"]
        except KeyError:
            return True
        hashtags = []
        for hashtag in dict_data["entities"]["hashtags"]:
            tempdict = {"hashtag": hashtag["text"]}
            hashtags.append(tempdict)

        if 'retweeted_status' in dict_data:
            for hashtag in dict_data['retweeted_status']['entities']['hashtags']:
                tempdict = {"hashtag": hashtag["text"]}
                hashtags.append(tempdict)

        urltags = []
        for url in dict_data["entities"]["urls"]:
            tempdict = {"url": url["url"]}
            urltags.append(tempdict)
            #print(hashtag["text"])
        temp_text = re.sub(r'RT(\s@\w+:)|http\S+|[^A-Za-z\s]','',text)
        pos_value,neg_value = senti_classifier.polarity_scores([temp_text])
        temp_dict1['senti-classifier-positive-score'] = pos_value
        temp_dict1['senti-classifier-negative-score'] = neg_value
        temp_dict2['sentiment-classifier-statement'] = "Positive" if((pos_value-neg_value) > 0) else "Neutral" if((pos_value-neg_value)==0) else "Negative"
        dict_data['sentiment'] = temp_dict2['sentiment-classifier-statement']
        
        coordinates=[]
        randX=random.random()*8
        randY=random.random()*8
        boundingX=22.3
        boundingY=72.5
        print(text)
        print(dict_data['text'])
        print(dict_data['created_at'])
        no_next_line_text = dict_data['retweeted_status']['extended_tweet']['full_text']             if 'retweeted_status' in dict_data and dict_data['retweeted_status']['truncated'] else dict_data['text']
        no_next_line_text = no_next_line_text.replace('\n',' ')
        requiredJSONbody={"screen_name": dict_data["user"]["screen_name"],
                       "user_name": dict_data["user"]["name"],
                       "user_id": dict_data["user"]["id_str"],
                       "followers_count": dict_data["user"]["followers_count"],
                       "is_user_verified": dict_data["user"]["verified"],
                       "original_user_name": dict_data['retweeted_status']['user']['name'] if 'retweeted_status' in dict_data else "",
                       "original_user_id": dict_data['retweeted_status']['user']["id_str"]  if 'retweeted_status' in dict_data else "",
                       "original_user_followers_count": dict_data['retweeted_status']['user']["followers_count"]  if 'retweeted_status' in dict_data else "",
                       "is_original_user_verified": dict_data['retweeted_status']['user']["verified"]  if 'retweeted_status' in dict_data else "",
                       "location": dict_data["user"]["location"],
                       "retweeted": dict_data["retweeted"],
                       "created_at": dt.strptime(dict_data['created_at'], "%a %b %d %H:%M:%S %z %Y")\
                       .strftime("%Y-%M-%d %H:%M:%S"),
                       "hashtags": hashtags,
                       "favorite_count": dict_data["favorite_count"],
                       "retweet_count": dict_data["retweet_count"],
                       "sentiment": dict_data["sentiment"],
                       "message": no_next_line_text,
                        # "geo": dict_data["geo"],
                        # "coordinates": coordinates,
                        # "coordinates":dict_data["coordinates"],
                        # "place": dict_data["place"],
                        # "user_mentions_name": dict_data["entities"]["user_mentions"],
                          # "urls":urltags,
                      }

        with open(tweet_csv, 'r+') as outfile:
            reader = csv.reader(outfile)
            flag = False
            headers = sorted([k for k, v in requiredJSONbody.items()])
            csv_data = []
            for row in reader:
                if row == headers:
                    flag = True
                break

            csv_data.append([requiredJSONbody[a] for a in headers])
            writer = csv.writer(outfile)
            if not flag:
                writer.writerows([headers])
            writer.writerows([csv_data[0]])

        tweets_data = pd.read_csv(tweet_csv, encoding='utf-8')
        # unique_users = tweets_data.groupby(['original_user_name', 'message'])
        self.compare_handlers(tweets_data)
        print("done")
        index = "twit"
        document_type = "tweets"

    def on_error(self, status):
        print("here")
        print(status)


consumer_key = 'kdcOWlnYEx10Oqi4VcKM6Gjam'
consumer_secret = 'sJVTx602Q0kgIiDIqVd78eeIFuUpYFLOuisfLLm1ZY2JT6D5pU'
access_token = '794180330462687232-kDBYdgJ19YppIhzSuoxDWwEUhgaFLkW'
access_token_secret = '9uBc1ugcO3DB5bWZVUzLgR1Kel3RxuohXVy2w1p2u6w3q'


listener = TweetStreamListener()
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
with open(tweet_csv, 'w') as outfile:
    writer = csv.writer(outfile)
    writer.writerows("")
    outfile.close()

while True:
    try:
        stream = Stream(auth, listener)
        stream.filter(follow=to_follow)        #locations= location)
    except ProtocolError:
        continue
    except IncompleteRead:
        continue
    except KeyboardInterrupt:
        stream.disconnect()
    break

