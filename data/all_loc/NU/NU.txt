4036203	Vili	Vili		-19.06703	-169.78799	P	PPL	NU		00				0		59	Pacific/Niue	2016-06-28
4036204	Vaohina	Vaohina		-18.99915	-169.90451	L	LCTY	NU		00				0		71	Pacific/Niue	2016-06-28
4036205	Vailoa Chasm	Vailoa Chasm		-19	-169.91667	T	GRGE	NU		00				0		1	Pacific/Niue	1993-12-22
4036206	Vaiila	Vaiila		-19.01667	-169.91667	L	LCTY	NU		00				0		74	Pacific/Niue	1993-12-22
4036207	Vahavaha	Vahavaha		-19.10057	-169.81499	P	PPL	NU		00				0		66	Pacific/Niue	2015-08-31
4036208	Tutuila	Tutuila		-18.95	-169.9	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036209	Tumukieto	Tumukieto	Tumukieto	-19.13199	-169.86233	V	SCRB	NU		00				0		1	Pacific/Niue	2018-05-09
4036210	Tumea	Tumea		-18.98333	-169.8	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036211	Tukitukipule	Tukitukipule		-19	-169.91667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036212	Tuapa Village	Tuapa Village	Tuapa,Tuapa Village	-18.99149	-169.90422	P	PPL	NU		00				129		2	Pacific/Niue	2018-05-09
4036213	Tomb Point	Tomb Point	Tomb Point	-19.05442	-169.92188	T	PT	NU		00				0		51	Pacific/Niue	2018-05-09
4036214	Toi	Toi	Toi	-18.97399	-169.85327	P	PPL	NU		00				0		63	Pacific/Niue	2016-06-28
4036215	Toa	Toa		-19.05	-169.91667	L	LCTY	NU		00				0		38	Pacific/Niue	1993-12-22
4036216	Tipa	Tipa	Blow Hole Point,Tipa	-19.1	-169.91667	T	PT	NU	NU	00				0		77	Pacific/Niue	2012-01-18
4036217	Tiatele	Tiatele		-18.95	-169.81667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036218	Tialeuta	Tialeuta		-18.95	-169.85	L	LCTY	NU		00				0		2	Pacific/Niue	1993-12-22
4036219	Taumalala	Taumalala		-19.05	-169.78333	L	LCTY	NU		00				0		55	Pacific/Niue	1993-12-22
4036220	Taueau	Taueau		-19	-169.91667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036221	Tapaike	Tapaike		-18.95	-169.86667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036222	Taoli	Taoli		-19.1	-169.88333	P	PPL	NU		00				0		57	Pacific/Niue	1993-12-22
4036223	Tamatakula	Tamatakula		-18.95	-169.88333	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036224	Tamakautoga	Tamakautoga	Tamakautoga	-19.0899	-169.91863	L	LCTY	NU		00				0		62	Pacific/Niue	2016-06-28
4036225	Talakifea	Talakifea		-19.01667	-169.91667	L	LCTY	NU		00				0		74	Pacific/Niue	1993-12-22
4036226	Takaoga	Takaoga	Takaoga,Takaogo	-19.02386	-169.81293	L	LCTY	NU	NU	00				0		66	Pacific/Niue	2016-06-28
4036227	Tahileleki	Tahileleki		-19	-169.91667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036228	Pikiona	Pikiona		-19.0663	-169.82404	P	PPL	NU		00				0		46	Pacific/Niue	2016-06-28
4036229	Patuoko	Patuoko		-18.97152	-169.81417	L	LCTY	NU		00				0		67	Pacific/Niue	2015-08-31
4036230	Nukufetau	Nukufetau		-19.07891	-169.84923	P	PPL	NU		00				0		59	Pacific/Niue	2015-08-31
4036231	Niue Island	Niue Island	Ile de Niue,Iniue,Niue,Niue Island,Savage Island	-19.033	-169.866	T	ISL	NU	NU	00				0		67	Pacific/Niue	2017-01-27
4036232	Niue	Niue	IUE,Isla Niue,Nie,Nija,Nijue,Nioe,Nioue,Nioué,Nioé,Niue,Niue Adalari,Niue Adaları,Niue Arili,Niue nutome,Niueh,Niueh araly,Niui,Niuje,Niujė,Niuo,Niuwe,Niuè,Niué,Niué Arili,Niuē,Nive,Niwe,Niye,Niyu,Niyuwe,Niûe,Nju,Nyue,Nyuwe,Nyué,Nívé,Orileede Niue,Orílẹ́ède Niue,Republic of Niue,i-Niue,n'yu'e,n'yuve,ni xu xe,ni'u,ni'u'e,ni'uye,niu ai,niue,niue dao,niyu,niyu'i,niyuve,nu'u,nyway,nywwh,nywwy,nywy,nywyh,nyyw,nyywy,Νιούε,Ние,Ниуе,Ниуэ,Ниуэ аралы,Нијуе,Ніуе,Ніуэ,Нія,Նիուե,ניאו,ניואה,ניווה,نيواى,نيوي,نييوي,نیئو,نیوئه,نیووی,نیووے,ނީއު,नियुइ,नीयू,न्युए,নিউয়ে,নুউ,ਨਿਊਏ,નીયુ,ନିଉ,நியுவே,நியூ,నియు,ನಿಯು,നിയുവെ,ന്യൂവേ,නියූ,นีอูเอ,ນີຢູ,ནིའུ་ཝ།,ნიუე,ኒኡይ,ニウエ,ニウエ島,紐埃,纽埃,니우에	-19.03333	-169.86667	A	PCLS	NU	NU	00				2166		65	Pacific/Niue	2017-06-02
4036233	Natuku	Natuku		-19.03333	-169.78333	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036234	Mutalau	Mutalau	Mutalau,Mutalau Village	-18.96226	-169.82826	P	PPL	NU		00				133		24	Pacific/Niue	2018-05-09
4036235	Muilimu	Muilimu		-18.95	-169.9	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036236	Motutapu	Motutapu	Motutapu,Motutopu	-19.034	-169.867	L	LCTY	NU	NU	00				0		65	Pacific/Niue	2017-01-27
4036237	Motule	Motule		-18.93333	-169.83333	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036238	Motu	Motu		-19.02962	-169.80143	P	PPL	NU		00				0		71	Pacific/Niue	2016-06-28
4036239	Meleuli	Meleuli		-18.95	-169.88333	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036240	Matavao	Matavao		-19.06918	-169.87996	P	PPL	NU		00				0		69	Pacific/Niue	2016-06-28
4036241	Matapa Chasm	Matapa Chasm		-18.95967	-169.87764	T	GRGE	NU		00				0		30	Pacific/Niue	2015-08-31
4036242	Matalave	Matalave		-18.95	-169.9	P	PPL	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036243	Matalakuti	Matalakuti		-18.95	-169.81667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036244	Matafonua	Matafonua		-18.95	-169.81667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036245	Mataafi	Mataafi		-19.11439	-169.84765	P	PPL	NU		00				0		51	Pacific/Niue	2015-08-31
4036246	Mamakula	Mamakula	Mamakula,Namukulu	-18.95	-169.9	P	PPL	NU		00				14		-9999	Pacific/Niue	2012-01-18
4036247	Makefu Village	Makefu Village	Makefu,Makefu Village	-19.0018	-169.91383	P	PPL	NU		00				87		3	Pacific/Niue	2018-05-09
4036248	Makatutaha	Makatutaha	Makatutaha	-18.9672	-169.88863	L	CST	NU		00				0		4	Pacific/Niue	2018-05-09
4036249	Makatugi	Makatugi		-19.08333	-169.91667	L	LCTY	NU		00				0		60	Pacific/Niue	1993-12-22
4036250	Makatu	Makatu		-18.98333	-169.91667	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036251	Makanga	Makanga		-18.99416	-169.81615	L	LCTY	NU		00				0		58	Pacific/Niue	2016-06-28
4036252	Makanga	Makanga		-18.95	-169.86667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036253	Makaapiapi	Makaapiapi		-19.1176	-169.90253	P	PPL	NU		00				0		68	Pacific/Niue	2016-06-28
4036254	Lolokieto	Lolokieto		-18.95	-169.88333	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036255	Liku Village	Liku Village	Liku,Liku Village	-19.05417	-169.78857	P	PPL	NU		00				71		1	Pacific/Niue	2018-05-09
4036256	Liha	Liha		-18.95	-169.8	P	PPL	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036257	Lavaka	Lavaka		-18.95	-169.81667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036258	Lalotuake	Lalotuake		-19.01667	-169.91667	L	LCTY	NU		00				0		74	Pacific/Niue	1993-12-22
4036259	Lalole	Lalole	Lalole	-18.99417	-169.81133	V	FRST	NU		00				0		1	Pacific/Niue	2018-05-09
4036260	Laloata	Laloata		-18.95	-169.88333	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036261	Lakepa	Lakepa	Lakepa	-18.98333	-169.8	P	PPL	NU		00				88		-9999	Pacific/Niue	2013-08-18
4036262	Kavaka	Kavaka		-19.03333	-169.78333	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036263	Kaupa	Kaupa	Kaopa,Kaupa	-19.04055	-169.79128	V	FRST	NU		00				0		1	Pacific/Niue	2018-05-09
4036264	Kaimite	Kaimite		-19.05	-169.91667	L	LCTY	NU		00				0		38	Pacific/Niue	1993-12-22
4036265	Huvalu	Huvalu		-19.08856	-169.82383	L	LCTY	NU		00				0		62	Pacific/Niue	2016-06-28
4036266	Hina	Hina		-18.95	-169.9	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036267	Hikutivake	Hikutivake		-18.96478	-169.88339	P	PPL	NU		00				63		34	Pacific/Niue	2015-08-31
4036268	Hakupu	Hakupu	Hakupu	-19.12753	-169.84623	P	PPL	NU		00				221		54	Pacific/Niue	2015-08-31
4036269	Grutulili	Grutulili		-18.96667	-169.81667	L	LCTY	NU		00				0		73	Pacific/Niue	1993-12-22
4036270	Fumaila	Fumaila		-18.95	-169.88333	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036271	Fulalatea	Fulalatea		-19.10381	-169.89532	P	PPL	NU		00				0		56	Pacific/Niue	2016-06-28
4036272	Fugigie	Fugigie		-19	-169.91667	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
4036273	Fuata	Fuata		-19.0609	-169.89404	P	PPL	NU		00				0		61	Pacific/Niue	2016-06-28
4036274	Fatiau	Fatiau		-19.11667	-169.9	P	PPL	NU		00				60		65	Pacific/Niue	2006-01-27
4036275	Falehavaiki	Falehavaiki		-19.1	-169.86667	L	LCTY	NU		00				0		41	Pacific/Niue	1993-12-22
4036276	Fakaagi	Fakaagi		-19.1	-169.9	L	LCTY	NU		00				0		48	Pacific/Niue	1993-12-22
4036277	Fagalilika	Fagalilika		-19.06792	-169.91065	P	PPL	NU		00				0		73	Pacific/Niue	2016-06-28
4036278	Avatele Bay	Avatele Bay	Avatele Bay	-19.11962	-169.9173	H	BAY	NU		00				0		28	Pacific/Niue	2015-08-31
4036279	Avatele	Avatele	Avatele,Matavaihala,Oneonepata	-19.1	-169.91667	P	PPL	NU		00				150		77	Pacific/Niue	2013-08-18
4036280	Anakula	Anakula		-19.01667	-169.91667	L	LCTY	NU		00				0		74	Pacific/Niue	1993-12-22
4036281	Anaheke	Anaheke		-18.98333	-169.91667	L	LCTY	NU		00				0		-9999	Pacific/Niue	1993-12-22
4036282	Ana	Ana		-19.06792	-169.80499	P	PPL	NU		00				0		66	Pacific/Niue	2016-06-28
4036283	Alofi Bay	Alofi Bay	Alofi Bay	-19.04244	-169.92569	H	BAY	NU		00				0		11	Pacific/Niue	2018-05-09
4036284	Alofi	Alofi	Alofi,Alofis,Alofo,Pasjolak Alofi,a luo fei,alaphi,allopi,alophi,alopi,alwfy,arofi,xalofi,Αλόφι,Алофи,Алофі,Пасёлак Алофі,الوفی,अलोफी,അലാഫി,อาโลฟี,ალოფი,アロフィ,阿洛菲,알로피	-19.05451	-169.91768	P	PPLC	NU		00				624		44	Pacific/Niue	2016-06-28
4036285	Aleuta	Aleuta		-19.05	-169.95	L	LCTY	NU		00				0		1	Pacific/Niue	1993-12-22
6299957	Alofi / Niue	Alofi / Niue	IUE,NIUE	-19.08003	-169.92564	S	AIRP	NU						0	63	60	Pacific/Niue	2012-01-27
10630010	Mutalau	Mutalau		-18.96206	-169.82954	P	PPL	NU						0		74	Pacific/Niue	2018-12-01
10630011	Matapa Chasm	Matapa Chasm		-18.96241	-169.88203	T	VAL	NU						0		29	Pacific/Niue	2015-08-31
10630012	Liku	Liku		-19.05388	-169.78971	P	PPL	NU						0		59	Pacific/Niue	2018-12-01
10630013	Tautu Beach	Tautu Beach		-19.04868	-169.78145	T	BCH	NU						0		13	Pacific/Niue	2015-08-31
10630014	Niue Island	Niue Island		-19.07218	-169.85447	L	LCTY	NU						0		53	Pacific/Niue	2015-08-31
10630015	Avatele	Avatele		-19.12449	-169.91138	P	PPL	NU						0		29	Pacific/Niue	2018-12-01
10630016	Avatele Beach	Avatele Beach		-19.12676	-169.91236	T	BCH	NU						0		17	Pacific/Niue	2015-08-31
11185964	Motutapu	Motutapu		-19.02967	-169.88683	P	PPL	NU						0		54	Pacific/Niue	2016-06-28
11185965	Lalotuake	Lalotuake		-19.01206	-169.91335	P	PPL	NU		00				0		84	Pacific/Niue	2016-06-28
11185966	Makefu Village	Makefu Village		-19.00216	-169.91429	P	PPL	NU						0		31	Pacific/Niue	2018-12-01
11185967	Tuapa Village	Tuapa Village		-18.99266	-169.90391	P	PPL	NU						0		33	Pacific/Niue	2018-12-01
11185968	Tahileleki	Tahileleki		-18.98747	-169.89103	P	PPL	NU						0		77	Pacific/Niue	2016-06-28
11185969	Namukulu Village	Namukulu Village		-18.98361	-169.89803	P	PPL	NU						0		38	Pacific/Niue	2018-12-01
11185970	Hio Beach	Hio Beach		-18.98864	-169.90421	T	BCH	NU						0		8	Pacific/Niue	2016-06-28
11185971	Patuoko	Patuoko		-18.9714	-169.81293	P	PPL	NU						0		69	Pacific/Niue	2016-06-28
11185972	Makanga	Makanga		-18.99514	-169.81301	P	PPL	NU						0		61	Pacific/Niue	2016-06-28
11185973	Lakepa Village	Lakepa Village		-19.01056	-169.80589	P	PPL	NU						0		56	Pacific/Niue	2018-12-01
11185974	Takaoga	Takaoga		-19.01555	-169.81009	P	PPL	NU						0		70	Pacific/Niue	2016-06-28
11185975	Huvalu Forest Conservation Area	Huvalu Forest Conservation Area		-19.08199	-169.81816	L	RESN	NU						0		60	Pacific/Niue	2016-06-28
11185976	Falehavaiki	Falehavaiki		-19.10365	-169.86897	P	PPL	NU						0		58	Pacific/Niue	2016-06-28
11185977	Vaiea	Vaiea		-19.12936	-169.88382	P	PPL	NU						0		55	Pacific/Niue	2018-12-01
11185978	Tamakautoga	Tamakautoga		-19.09923	-169.91876	P	PPL	NU						0		68	Pacific/Niue	2018-12-01
11185979	Huihui	Huihui		-19.07242	-169.9315	P	PPLX	NU						0		72	Pacific/Niue	2016-06-28
11185980	Niue Post Office	Niue Post Office		-19.05409	-169.9203	S	PO	NU						0		25	Pacific/Niue	2016-06-28
11185981	Toa	Toa		-19.04403	-169.89957	P	PPL	NU						0		57	Pacific/Niue	2016-06-28
11869863	Wash Away Café	Wash Away Cafe	Wash Away Cafe,Wash Away Café	-19.12803	-169.91232	S	BLDG	NU		00				0		34	Pacific/Niue	2018-05-12
11869864	Vinivini-Motoliku Bush Road	Vinivini-Motoliku Bush Road	Vinivini-Motoliku Bush Road	-19.07778	-169.83108	R	RD	NU		00				0		11	Pacific/Niue	2018-05-12
11869865	Vinivini	Vinivini	Vinivini	-19.06117	-169.81818	V	FRST	NU		00				0		19	Pacific/Niue	2018-05-12
11869866	Veve	Veve	Veve	-19.10052	-169.85145	S	CAVE	NU		00				0		7	Pacific/Niue	2018-05-12
11869867	Vaotoi	Vaotoi	Vaotoi	-19.11551	-169.8177	S	CAVE	NU		00				0		1	Pacific/Niue	2018-05-12
11869868	Valikulu Sea Track	Valikulu Sea Track	Valikulu Sea Track	-19.06615	-169.78916	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869869	Valikulu	Valikulu	Valikulu	-19.06912	-169.77731	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869870	Vaitanetane Bush Road	Vaitanetane Bush Road	Vaitanetane Bush Road	-19.00379	-169.84223	R	TRL	NU		00				0		38	Pacific/Niue	2018-05-12
11869871	Vaitafe Sea Track	Vaitafe Sea Track	Vaitafe Sea Track	-18.99124	-169.81056	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869872	Vaitafe	Vaitafe	Vaitafe	-18.98685	-169.80219	T	BCH	NU		00				0		1	Pacific/Niue	2018-05-12
11869873	Vaipukupuku Bush Road	Vaipukupuku Bush Road	Vaipukupuku Bush Road	-18.9937	-169.86851	R	TRL	NU		00				0		28	Pacific/Niue	2018-05-12
11869874	Vaipapahi Farm	Vaipapahi Farm	Vaipapahi Farm	-18.97781	-169.87429	S	FRM	NU		00				0		21	Pacific/Niue	2018-05-12
11869875	Vaiopeope	Vaiopeope	Vaiopeope	-18.95361	-169.83916	L	CST	NU		00				0		7	Pacific/Niue	2018-05-12
11869898	Vailoa	Vailoa	Vailoa	-19.03664	-169.91675	S	CAVE	NU		00				0		10	Pacific/Niue	2018-05-12
11869899	Vaila	Vaila	Vaila	-19.04411	-169.91789	L	CST	NU		00				0		26	Pacific/Niue	2018-05-12
11869900	Vaikona	Vaikona	Vaikona	-19.08687	-169.78904	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869901	Vaihoko	Vaihoko	Vaihoko	-18.95308	-169.8523	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11869902	Vaihakea	Vaihakea	Vaihakea	-18.95758	-169.82216	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11869903	Vaigata	Vaigata	Vaigata	-19.06378	-169.77474	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869904	Vaifulufulu	Vaifulufulu	Vaifulufulu	-19.05282	-169.80291	V	FRST	NU		00				0		11	Pacific/Niue	2018-05-12
11869905	Vaiea-Kulukulu Bush Road	Vaiea-Kulukulu Bush Road	Vaiea-Kulukulu Bush Road	-19.13302	-169.88751	R	RD	NU		00				0		4	Pacific/Niue	2018-05-12
11869906	Vaiea Nonu Farm	Vaiea Nonu Farm	Vaiea Nonu Farm	-19.12978	-169.88138	S	FRM	NU		00				0		2	Pacific/Niue	2018-05-12
11869907	Vahigano	Vahigano	Vahigano	-19.14237	-169.84037	S	CAVE	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869908	Vaha Bush Road	Vaha Bush Road	Vaha Bush Road	-19.02548	-169.82921	R	TRL	NU		00				0		42	Pacific/Niue	2018-05-12
11869909	Vaha	Vaha	Vaha	-19.02685	-169.82871	V	FRST	NU		00				0		42	Pacific/Niue	2018-05-12
11869910	Va	Va	Va	-19.0713	-169.86012	V	SCRB	NU		00				0		24	Pacific/Niue	2018-05-12
11869911	Uluvehi Sea Track	Uluvehi Sea Track	Uluvehi Sea Track	-18.96166	-169.82876	R	TRL	NU		00				0		24	Pacific/Niue	2018-05-12
11869912	Uluvehi	Uluvehi	Uluvehi	-18.95649	-169.82855	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11869913	Ulupaka	Ulupaka	Ulupaka	-19.02202	-169.80142	S	CAVE	NU		00				0		9	Pacific/Niue	2018-05-12
11869914	Uani Sea Track	Uani Sea Track	Uani Sea Track	-19.12236	-169.82518	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869915	Tutu Bush Road	Tutu Bush Road	Tutu Bush Road	-18.98401	-169.86012	R	TRL	NU		00				0		33	Pacific/Niue	2018-05-12
11869916	Tutu	Tutu	Tutu	-19.11068	-169.85426	V	FRST	NU		00				0		4	Pacific/Niue	2018-05-12
11869917	Tusekolo	Tusekolo	Tusekolo	-19.05076	-169.89523	V	FRST	NU		00				0		39	Pacific/Niue	2018-05-12
11869918	Tuo Sea Track	Tuo Sea Track	Tuo Sea Track	-18.96315	-169.82618	R	TRL	NU		00				0		24	Pacific/Niue	2018-05-12
11869919	Tuo	Tuo	Tuo	-18.96379	-169.80744	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869920	Tumulou Bush Road	Tumulou Bush Road	Tumulou Bush Road	-19.09222	-169.87884	R	RD	NU		00				0		15	Pacific/Niue	2018-05-12
11869921	Tumulo	Tumulo	Tumulo	-19.08993	-169.87994	V	FRST	NU		00				0		20	Pacific/Niue	2018-05-12
11869922	Tumukieto Bush Road	Tumukieto Bush Road	Tumukieto Bush Road	-19.13317	-169.86482	R	RD	NU		00				0		1	Pacific/Niue	2018-05-12
11869923	Tumukanumea	Tumukanumea	Tumukanumea	-18.99393	-169.84868	V	SCRB	NU		00				0		39	Pacific/Niue	2018-05-12
11869924	Tumufa-Paluki Bush Road	Tumufa-Paluki Bush Road	Tumufa-Paluki Bush Road	-19.08341	-169.88006	R	TRL	NU		00				0		20	Pacific/Niue	2018-05-12
11869925	Tumufa	Tumufa	Tumufa	-19.08296	-169.87625	V	SCRB	NU		00				0		25	Pacific/Niue	2018-05-12
11869926	Tukuofe	Tukuofe	Tukuofe	-19.0536	-169.84081	S	CAVE	NU		00				0		31	Pacific/Niue	2018-05-12
11869927	Tukumate	Tukumate	Tukumate	-18.96093	-169.81098	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11869928	Tuhia-atua	Tuhia-atua	Tuhia-atua	-19.13272	-169.83464	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869929	Tuhia Sea Track	Tuhia Sea Track	Tuhia Sea Track	-19.12908	-169.84211	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869930	Tuava Bush Track	Tuava Bush Track	Tuava Bush Track	-18.95815	-169.84557	R	TRL	NU		00				0		2	Pacific/Niue	2018-05-12
11869931	Tuapa Bush Road	Tuapa Bush Road	Tuapa Bush Road	-19.00934	-169.86706	R	TRL	NU		00				0		31	Pacific/Niue	2018-05-12
11869932	Tualihega Bush Road	Tualihega Bush Road	Tualihega Bush Road	-19.08119	-169.87292	R	RD	NU		00				0		23	Pacific/Niue	2018-05-12
11869933	Tokamea	Tokamea	Tokamea	-19.03945	-169.9101	S	HSTS	NU		00				0		19	Pacific/Niue	2018-05-12
11869934	Toi-Mutalau Road	Toi-Mutalau Road	Toi-Mutalau Road	-18.97418	-169.86171	R	RD	NU		00				0		22	Pacific/Niue	2018-05-12
11869935	Toi Sea Track	Toi Sea Track	Toi Sea Track	-18.97206	-169.8488	R	TRL	NU		00				0		46	Pacific/Niue	2018-05-12
11869936	Togo	Togo	Togo	-19.1048	-169.80707	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869937	Togatiti	Togatiti	Togatiti	-19.06206	-169.87583	V	FRST	NU		00				0		32	Pacific/Niue	2018-05-12
11869938	Togalupo	Togalupo	Togalupo	-19.0472	-169.91842	L	CST	NU		00				0		26	Pacific/Niue	2018-05-12
11869939	Togakaho	Togakaho	Togakaho	-19.04194	-169.86821	V	FRST	NU		00				0		32	Pacific/Niue	2018-05-12
11869940	Tavahihi	Tavahihi	Tavahihi	-18.99146	-169.90624	L	CST	NU		00				0		2	Pacific/Niue	2018-05-12
11869941	Tauli	Tauli	Tauli	-19.01485	-169.83013	V	FRST	NU		00				0		29	Pacific/Niue	2018-05-12
11869942	Tatapiu Bush Road	Tatapiu Bush Road	Tatapiu Bush Road	-19.0778	-169.89095	R	RD	NU		00				0		28	Pacific/Niue	2018-05-12
11869943	Taputapu	Taputapu	Taputapu	-19.06194	-169.88818	V	FRST	NU		00				0		36	Pacific/Niue	2018-05-12
11869944	Taoke Sea Track	Taoke Sea Track	Taoke Sea Track	-19.01131	-169.80408	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869945	Taoke	Taoke	Taoke	-19.0072	-169.79414	T	BCH	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869946	Tamani Track	Tamani Track	Tamani Track	-19.13837	-169.8563	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869947	Talomili Bush Road	Talomili Bush Road	Talomili Bush Road	-18.98332	-169.88925	R	RD	NU		00				0		8	Pacific/Niue	2018-05-12
11869948	Taloli	Taloli	Taloli	-19.13272	-169.87815	V	SCRB	NU		00				0		2	Pacific/Niue	2018-05-12
11869949	Taloa Heights	Taloa Heights	Taloa Heights	-19.05972	-169.9229	P	PPLX	NU		00				0		52	Pacific/Niue	2018-05-12
11869950	Talava Sea Track	Talava Sea Track	Talava Sea Track	-18.96075	-169.87891	R	TRL	NU		00				0		3	Pacific/Niue	2018-05-12
11869951	Talava Arches	Talava Arches	Talava Arches	-18.96013	-169.87931	S	ARCH	NU		00				0		3	Pacific/Niue	2018-05-12
11869952	Talamaitoga Bush Road	Talamaitoga Bush Road	Talamaitoga Bush Road	-19.12696	-169.89615	R	RD	NU		00				0		7	Pacific/Niue	2018-05-12
11869953	Talamaitoga	Talamaitoga	Talamaitoga	-19.12649	-169.89593	P	PPL	NU		00				0		7	Pacific/Niue	2018-05-12
11869954	Tafolomahina Bush Road	Tafolomahina Bush Road	Tafolomahina Bush Road	-19.04438	-169.86632	R	TRL	NU		00				0		33	Pacific/Niue	2018-05-12
11869955	Tafolomahina	Tafolomahina	Tafolomahina	-19.04677	-169.85474	V	FRST	NU		00				0		34	Pacific/Niue	2018-05-12
11869956	Tafalalo	Tafalalo	Tafalalo	-19.06797	-169.94242	L	CST	NU		00				0		5	Pacific/Niue	2018-05-12
11869957	Stone Villa	Stone Villa	Stone Villa	-19.05551	-169.92031	P	PPLX	NU		00				0		51	Pacific/Niue	2018-05-12
11869958	Sir Robert Rex Wharf	Sir Robert Rex Wharf	Sir Robert Rex Wharf	-19.0527	-169.92091	S	WHRF	NU		00				0		51	Pacific/Niue	2018-05-12
11869959	Seikena	Seikena	Seikena	-19.01716	-169.91782	S	TOWR	NU		00				0		2	Pacific/Niue	2018-05-12
11869960	Segisegi Bush Track	Segisegi Bush Track	Segisegi Bush Track	-19.01938	-169.87267	R	TRL	NU		00				0		32	Pacific/Niue	2018-05-12
11869961	Puluhiki Sea Track	Puluhiki Sea Track	Puluhiki Sea Track	-19.00942	-169.8066	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869962	Puluhiki	Puluhiki	Puluhiki	-19.00304	-169.79649	T	BCH	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869963	Porrit Drive	Porrit Drive	Porrit Drive	-19.10704	-169.91446	R	RD	NU		00				0		6	Pacific/Niue	2018-05-12
11869964	Pomea Bush Road	Pomea Bush Road	Pomea Bush Road	-18.99559	-169.85396	R	RD	NU		00				0		39	Pacific/Niue	2018-05-12
11869965	Pokoifi Track	Pokoifi Track	Pokoifi Track	-18.99208	-169.89934	R	TRL	NU		00				0		9	Pacific/Niue	2018-05-12
11869966	Pofitu	Pofitu	Pofitu	-19.11351	-169.91324	T	BCH	NU		00				0		10	Pacific/Niue	2018-05-12
11869967	Peniamina’s Graveyard	Peniamina's Graveyard	Peniamina's Graveyard,Peniamina’s Graveyard	-19.01465	-169.92332	S	GRVE	NU		00				0		1	Pacific/Niue	2018-05-12
11869968	Patuutu-Fukau Bush Road	Patuutu-Fukau Bush Road	Patuutu-Fukau Bush Road	-18.99338	-169.83915	R	RD	NU		00				0		34	Pacific/Niue	2018-05-12
11869969	Patuutu	Patuutu	Patuutu	-18.99355	-169.84042	V	SCRB	NU		00				0		34	Pacific/Niue	2018-05-12
11869970	Patuoku Sea Track	Patuoku Sea Track	Patuoku Sea Track	-18.9725	-169.81999	R	TRL	NU		00				0		9	Pacific/Niue	2018-05-12
11869971	Palaha	Palaha	Palaha	-18.99547	-169.90888	S	CAVE	NU		00				0		1	Pacific/Niue	2018-05-12
11869972	Pala Sea Track	Pala Sea Track	Pala Sea Track	-19.14816	-169.85972	R	TRL	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869973	Pala	Pala	Pala	-19.14888	-169.86064	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869974	Pago-Panivaka Bush Road	Pago-Panivaka Bush Road	Pago-Panivaka Bush Road	-19.12662	-169.89924	R	RD	NU		00				0		7	Pacific/Niue	2018-05-12
11869975	Pagopago Bush Road	Pagopago Bush Road	Pagopago Bush Road	-19.11103	-169.85628	R	RD	NU		00				0		4	Pacific/Niue	2018-05-12
11869976	Opaahi	Opaahi	Opaahi	-19.06082	-169.93104	L	CST	NU		00				0		46	Pacific/Niue	2018-05-12
11869977	Oneiki	Oneiki	Oneiki	-19.09399	-169.90255	S	TOWR	NU		00				0		14	Pacific/Niue	2018-05-12
11869978	Omea	Omea	Omea	-19.00152	-169.88689	V	FRST	NU		00				0		18	Pacific/Niue	2018-05-12
11869979	Niumaga	Niumaga	Niumaga	-19.12906	-169.89555	V	SCRB	NU		00				0		7	Pacific/Niue	2018-05-12
11869980	Niufela	Niufela	Niufela	-19.0251	-169.83897	V	FRST	NU		00				0		41	Pacific/Niue	2018-05-12
11869981	Niuefela Bush Track	Niuefela Bush Track	Niuefela Bush Track	-19.02311	-169.8509	R	TRL	NU		00				0		38	Pacific/Niue	2018-05-12
11869982	Niue High School	Niue High School	Niue High School	-19.05666	-169.90967	S	SCH	NU		00				0		47	Pacific/Niue	2018-05-12
11869983	Namukulu Motel	Namukulu Motel	Namukulu Motel	-18.98319	-169.8967	S	HTL	NU		00				0		4	Pacific/Niue	2018-05-12
11869984	Namukulu Boat Ramp	Namukulu Boat Ramp	Namukulu Boat Ramp	-18.97634	-169.89725	H	BNKX	NU		00				0		4	Pacific/Niue	2018-05-12
11869985	Namuke Sea Track	Namuke Sea Track	Namuke Sea Track	-19.08092	-169.79691	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11869986	Namuke	Namuke	Namuke	-19.0851	-169.78779	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869987	Namoui Bush Road	Namoui Bush Road	Namoui Bush Road	-19.0198	-169.9219	R	RD	NU		00				0		2	Pacific/Niue	2018-05-12
11869988	Namoui	Namoui	Namoui	-19.01933	-169.923	T	BCH	NU		00				0		2	Pacific/Niue	2018-05-12
11869989	Motu Sea Track	Motu Sea Track	Motu Sea Track	-19.01871	-169.80381	R	TRL	NU		00				0		9	Pacific/Niue	2018-05-12
11869990	Matavai Resort	Matavai Resort	Matavai Resort	-19.1106	-169.91376	S	RSRT	NU		00				0		10	Pacific/Niue	2018-05-12
11869991	Matavai Motel	Matavai Motel	Matavai Motel	-19.10129	-169.92088	S	HTL	NU		00				0		1	Pacific/Niue	2018-05-12
11869992	Matakiu Bush Track	Matakiu Bush Track	Matakiu Bush Track	-19.09722	-169.85254	R	TRL	NU		00				0		10	Pacific/Niue	2018-05-12
11869993	Matakaulima	Matakaulima	Matakaulima	-19.01452	-169.7931	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869994	Mata Sea Track	Mata Sea Track	Mata Sea Track	-19.14542	-169.84263	R	TRL	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869995	Mata Point	Mata Point	Mata Point	-19.14629	-169.84286	T	PT	NU		00				0		-9999	Pacific/Niue	2018-05-12
11869996	Mana	Mana	Mana	-19.0517	-169.87857	V	FRST	NU		00				0		33	Pacific/Niue	2018-05-12
11869997	Maliekula	Maliekula	Maliekula	-19.11556	-169.8507	V	FRST	NU		00				0		4	Pacific/Niue	2018-05-12
11869998	Malekau-Makauga Road	Malekau-Makauga Road	Malekau-Makauga Road	-18.98513	-169.89984	R	RD	NU		00				0		7	Pacific/Niue	2018-05-12
11869999	Malefisi	Malefisi	Malefisi	-18.99915	-169.81982	V	FRST	NU		00				0		10	Pacific/Niue	2018-05-12
11870000	Malafati Bush Road	Malafati Bush Road	Malafati Bush Road	-19.03044	-169.82485	R	TRL	NU		00				0		42	Pacific/Niue	2018-05-12
11870001	Malafati	Malafati	Malafati	-19.03113	-169.82234	V	FRST	NU		00				0		42	Pacific/Niue	2018-05-12
11870002	Makavilitoga	Makavilitoga	Makavilitoga	-19.03492	-169.83491	V	FRST	NU		00				0		40	Pacific/Niue	2018-05-12
11870003	Makauga	Makauga	Makauga	-18.97363	-169.86729	V	SCRB	NU		00				0		14	Pacific/Niue	2018-05-12
11870004	Makatu	Makatu	Makatu	-19.00312	-169.85959	V	SCRB	NU		00				0		35	Pacific/Niue	2018-05-12
11870005	Makato	Makato	Makato	-19.03791	-169.91652	S	CAVE	NU		00				0		19	Pacific/Niue	2018-05-12
11870006	Makapu Point	Makapu Point	Makapu Point	-19.01442	-169.92407	T	PT	NU		00				0		1	Pacific/Niue	2018-05-12
11870007	Makapu	Makapu	Makapu	-19.01397	-169.92375	S	CAVE	NU		00				0		1	Pacific/Niue	2018-05-12
11870008	Makamalu	Makamalu	Makamalu	-19.01133	-169.82811	V	SCRB	NU		00				0		29	Pacific/Niue	2018-05-12
11870009	Makalea	Makalea	Makalea	-18.96912	-169.8902	S	CAVE	NU		00				0		4	Pacific/Niue	2018-05-12
11870010	Maila	Maila	Maila	-19.0138	-169.8177	V	FRST	NU		00				0		20	Pacific/Niue	2018-05-12
11870011	Lou	Lou	Lou	-19.0992	-169.79925	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870012	Loki Bush Road	Loki Bush Road	Loki Bush Road	-18.99826	-169.89184	R	RD	NU		00				0		9	Pacific/Niue	2018-05-12
11870013	Logovau	Logovau	Logovau	-19.00877	-169.86532	V	FRST	NU		00				0		35	Pacific/Niue	2018-05-12
11870014	Logovao-Fugavai Bush Road	Logovao-Fugavai Bush Road	Logovao-Fugavai Bush Road	-19.0273	-169.85317	R	RD	NU		00				0		37	Pacific/Niue	2018-05-12
11870015	Limufuafua Point	Limufuafua Point	Limufuafua Point	-19.1554	-169.87879	T	PT	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870016	Limu	Limu	Limu	-18.97495	-169.8962	H	POOL	NU		00				0		1	Pacific/Niue	2018-05-12
11870017	Liha Point	Liha Point	Liha Point	-18.96879	-169.80444	T	PT	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870018	Leui	Leui	Leui	-19.05434	-169.77691	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11870019	Lefuka	Lefuka	Lefuka	-19.03609	-169.85446	V	FRST	NU		00				0		36	Pacific/Niue	2018-05-12
11870020	Lauila Bush Road	Lauila Bush Road	Lauila Bush Road	-19.13381	-169.85648	R	RD	NU		00				0		1	Pacific/Niue	2018-05-12
11870021	Lalokafika Road	Lalokafika Road	Lalokafika Road	-19.07708	-169.91373	R	RD	NU		00				0		32	Pacific/Niue	2018-05-12
11870022	Lalokafika	Lalokafika	Lalokafika	-19.07267	-169.89545	V	FRST	NU		00				0		37	Pacific/Niue	2018-05-12
11870023	Lalofika	Lalofika	Lalofika	-19.07245	-169.89558	V	FRST	NU		00				0		37	Pacific/Niue	2018-05-12
11870024	Kololi Motel	Kololi Motel	Kololi Motel	-19.05454	-169.91859	S	HTL	NU		00				0		51	Pacific/Niue	2018-05-12
11870025	Kings Lookout	Kings Lookout	Kings Lookout	-19.068	-169.93578	S	HSTS	NU		00				0		18	Pacific/Niue	2018-05-12
11870026	Kenakena Bush Road	Kenakena Bush Road	Kenakena Bush Road	-19.09369	-169.87043	R	RD	NU		00				0		14	Pacific/Niue	2018-05-12
11870027	Keleola Bush Road	Keleola Bush Road	Keleola Bush Road	-19.05208	-169.88721	R	RD	NU		00				0		36	Pacific/Niue	2018-05-12
11870028	Keleola	Keleola	Keleola	-19.05078	-169.88627	V	FRST	NU		00				0		36	Pacific/Niue	2018-05-12
11870029	Kavata	Kavata	Kavata	-19.08199	-169.88988	S	CAVE	NU		00				0		28	Pacific/Niue	2018-05-12
11870030	Kavata	Kavata	Kavata	-18.95823	-169.87302	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11870031	Kauhi	Kauhi	Kauhi	-19.03208	-169.78981	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870032	Kapika	Kapika	Kapika	-19.09506	-169.86679	V	SCRB	NU		00				0		14	Pacific/Niue	2018-05-12
11870033	Kapihi Bush Road	Kapihi Bush Road	Kapihi Bush Road	-18.99001	-169.844	R	RD	NU		00				0		42	Pacific/Niue	2018-05-12
11870034	Kapihi	Kapihi	Kapihi	-18.97445	-169.83845	V	CULT	NU		00				0		52	Pacific/Niue	2018-05-12
11870035	Kakaoka	Kakaoka	Kakaoka	-19.15052	-169.86604	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870036	Kaimiti-Hulitoga Bush Road	Kaimiti-Hulitoga Bush Road	Kaimiti-Hulitoga Bush Road	-19.07464	-169.91321	R	RD	NU		00				0		43	Pacific/Niue	2018-05-12
11870037	Kaho	Kaho	Kaho	-19.13234	-169.9089	S	TOWR	NU		00				0		34	Pacific/Niue	2018-05-12
11870038	Huvalu Forest	Huvalu Forest	Huvalu Forest	-19.08338	-169.81125	V	FRST	NU		00				0		1	Pacific/Niue	2018-05-12
11870039	Huimaui	Huimaui	Huimaui	-19.00516	-169.88599	V	FRST	NU		00				0		18	Pacific/Niue	2018-05-12
11870040	Houma	Houma	Houma	-19.07041	-169.94586	S	CAVE	NU		00				0		5	Pacific/Niue	2018-05-12
11870041	Hiola Sea Track	Hiola Sea Track	Hiola Sea Track	-19.04025	-169.79362	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11870042	Hiola	Hiola	Hiola	-19.03746	-169.78735	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11870043	Hikutavake-Makauga Road	Hikutavake-Makauga Road	Hikutavake-Makauga Road	-18.96529	-169.88402	R	RD	NU		00				0		1	Pacific/Niue	2018-05-12
11870044	Hikulagi	Hikulagi	Hikulagi	-19.06386	-169.82891	V	FRST	NU		00				0		23	Pacific/Niue	2018-05-12
11870045	Heifi Bush Road	Heifi Bush Road	Heifi Bush Road	-19.03392	-169.91769	R	RD	NU		00				0		10	Pacific/Niue	2018-05-12
11870046	Havaka	Havaka	Havaka	-19.10298	-169.85937	V	FRST	NU		00				0		8	Pacific/Niue	2018-05-12
11870047	Haupo	Haupo	Haupo	-19.00603	-169.88245	V	FRST	NU		00				0		24	Pacific/Niue	2018-05-12
11870048	Hatiuga	Hatiuga	Hatiuga	-18.95665	-169.86669	L	CST	NU		00				0		1	Pacific/Niue	2018-05-12
11870049	Hatea	Hatea	Hatea	-18.97679	-169.81576	V	FRST	NU		00				0		1	Pacific/Niue	2018-05-12
11870050	Halavai	Halavai	Halavai	-19.05947	-169.77427	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870051	Halatafeta	Halatafeta	Halatafeta	-19.07805	-169.78284	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870052	Halakuma Bush Road	Halakuma Bush Road	Halakuma Bush Road	-19.10144	-169.86472	R	RD	NU		00				0		8	Pacific/Niue	2018-05-12
11870053	Halagigie Point	Halagigie Point	Halagigie Point	-19.07466	-169.9491	T	PT	NU		00				0		5	Pacific/Niue	2018-05-12
11870054	Halafualagi	Halafualagi	Halafualagi	-19.14679	-169.84697	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870055	Hago	Hago	Hago	-19.05815	-169.84724	V	FRST	NU		00				0		31	Pacific/Niue	2018-05-12
11870056	Hafata	Hafata	Hafata	-19.06007	-169.84206	V	FRST	NU		00				0		25	Pacific/Niue	2018-05-12
11870057	Gaga	Gaga	Gaga	-18.98982	-169.84455	V	FRST	NU		00				0		42	Pacific/Niue	2018-05-12
11870058	Fupao	Fupao	Fupao	-18.99868	-169.8436	V	FRST	NU		00				0		39	Pacific/Niue	2018-05-12
11870059	Fumati	Fumati	Fumati	-19.03544	-169.88646	V	FRST	NU		00				0		30	Pacific/Niue	2018-05-12
11870060	Fumalia Track	Fumalia Track	Fumalia Track	-19.13244	-169.84928	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11870061	Fukava	Fukava	Fukava	-18.98924	-169.86943	V	FRST	NU		00				0		26	Pacific/Niue	2018-05-12
11870062	Fukau	Fukau	Fukau	-18.97057	-169.82169	P	PPL	NU		00				0		9	Pacific/Niue	2018-05-12
11870063	Fugavai Bush Track	Fugavai Bush Track	Fugavai Bush Track	-19.02615	-169.84291	R	TRL	NU		00				0		39	Pacific/Niue	2018-05-12
11870064	Fufata Bush Track	Fufata Bush Track	Fufata Bush Track	-19.12261	-169.84888	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11870065	Fue-Kaupa Bush Road	Fue-Kaupa Bush Road	Fue-Kaupa Bush Road	-19.05228	-169.81925	R	RD	NU		00				0		27	Pacific/Niue	2018-05-12
11870066	Fue	Fue	Fue	-19.05123	-169.81922	V	FRST	NU		00				0		27	Pacific/Niue	2018-05-12
11870067	Fuata-Kinokino Bush Road	Fuata-Kinokino Bush Road	Fuata-Kinokino Bush Road	-19.11267	-169.88973	R	RD	NU		00				0		9	Pacific/Niue	2018-05-12
11870068	Fualahi	Fualahi	Fualahi	-19.07506	-169.92823	P	PPL	NU		00				0		22	Pacific/Niue	2018-05-12
11870069	Fou-Tusekolo Bush Road	Fou-Tusekolo Bush Road	Fou-Tusekolo Bush Road	-19.05183	-169.90622	R	TRL	NU		00				0		43	Pacific/Niue	2018-05-12
11870070	Foulua	Foulua	Foulua	-19.12724	-169.83023	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870071	Fou	Fou	Fou	-18.97667	-169.83935	V	CULT	NU		00				0		45	Pacific/Niue	2018-05-12
11870072	Fokipa Bush Road	Fokipa Bush Road	Fokipa Bush Road	-19.11299	-169.86597	R	TRL	NU		00				0		5	Pacific/Niue	2018-05-12
11870073	Fetuna Bush Track	Fetuna Bush Track	Fetuna Bush Track	-19.00247	-169.85952	R	RD	NU		00				0		35	Pacific/Niue	2018-05-12
11870074	Fetiki	Fetiki	Fetiki	-19.05006	-169.81437	V	FRST	NU		00				0		21	Pacific/Niue	2018-05-12
11870075	Fatuaua Falepipi	Fatuaua Falepipi	Fatuaua Falepipi	-18.99303	-169.89802	V	FRST	NU		00				0		9	Pacific/Niue	2018-05-12
11870076	Fatiau Tuai	Fatiau Tuai	Fatiau Tuai	-19.13847	-169.8896	V	FRST	NU		00				0		1	Pacific/Niue	2018-05-12
11870077	Fatiau Sea Track	Fatiau Sea Track	Fatiau Sea Track	-19.1435	-169.89294	R	TRL	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870078	Fatamau	Fatamau	Fatamau	-19.02458	-169.89565	V	FRST	NU		00				0		18	Pacific/Niue	2018-05-12
11870079	Fao Fao	Fao Fao	Fao Fao	-18.96912	-169.81277	S	CAVE	NU		00				0		1	Pacific/Niue	2018-05-12
11870080	Fafagu	Fafagu	Fafagu	-19.14036	-169.8894	S	HSTS	NU		00				0		1	Pacific/Niue	2018-05-12
11870081	Coral Garden Motel	Coral Garden Motel	Coral Garden Motel	-19.01328	-169.92305	S	HTL	NU		00				0		1	Pacific/Niue	2018-05-12
11870082	Avatele-Vaiea Road	Avatele-Vaiea Road	Avatele-Vaiea Road	-19.12749	-169.91137	R	RD	NU		00				0		34	Pacific/Niue	2018-05-12
11870083	Avatele Boat Ramp	Avatele Boat Ramp	Avatele Boat Ramp	-19.12609	-169.91304	H	BNKX	NU		00				0		34	Pacific/Niue	2018-05-12
11870084	Avaiki	Avaiki	Avaiki	-18.99741	-169.91102	S	CAVE	NU		00				0		1	Pacific/Niue	2018-05-12
11870085	Atafu Bush Road	Atafu Bush Road	Atafu Bush Road	-19.0925	-169.89673	R	RD	NU		00				0		16	Pacific/Niue	2018-05-12
11870086	Ata Bush Road	Ata Bush Road	Ata Bush Road	-19.00528	-169.89468	R	TRL	NU		00				0		12	Pacific/Niue	2018-05-12
11870087	Anokula	Anokula	Anokula	-19.07108	-169.94235	S	CAVE	NU		00				0		5	Pacific/Niue	2018-05-12
11870088	Anatoloa	Anatoloa	Anatoloa	-18.99639	-169.80702	S	CAVE	NU		00				0		1	Pacific/Niue	2018-05-12
11870089	Anatoga	Anatoga	Anatoga	-19.05392	-169.92088	S	CAVE	NU		00				0		51	Pacific/Niue	2018-05-12
11870090	Anapala	Anapala	Anapala	-19.13472	-169.83618	T	CFT	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870091	Anaiki Motel	Anaiki Motel	Anaiki Motel	-18.99749	-169.91055	S	HTL	NU		00				0		1	Pacific/Niue	2018-05-12
11870092	Anafetu	Anafetu	Anafetu	-18.97751	-169.80211	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870093	Ana Sea Track	Ana Sea Track	Ana Sea Track	-19.10936	-169.82398	R	TRL	NU		00				0		1	Pacific/Niue	2018-05-12
11870094	Ana	Ana	Ana	-19.11561	-169.81614	L	CST	NU		00				0		-9999	Pacific/Niue	2018-05-12
11870095	Ana	Ana	Ana	-19.02159	-169.92225	T	BCH	NU		00				0		2	Pacific/Niue	2018-05-12
11870096	Alofi Liku Road	Alofi Liku Road	Alofi Liku Road	-19.05594	-169.9213	R	RD	NU		00				0		51	Pacific/Niue	2018-05-12
11870097	Alofa-Lakepa Road	Alofa-Lakepa Road	Alofa-Lakepa Road	-19.0534	-169.87787	R	RD	NU		00				0		33	Pacific/Niue	2018-05-12
11870098	Ahuafi Bush Track	Ahuafi Bush Track	Ahuafi Bush Track	-19.09892	-169.86327	R	TRL	NU		00				0		12	Pacific/Niue	2018-05-12
11870099	Agoago Bush Track	Agoago Bush Track	Agoago Bush Track	-19.11987	-169.8518	R	TRL	NU		00				0		2	Pacific/Niue	2018-05-12
