4030690	Youngs Rock	Youngs Rock		-25.05749	-130.11278	T	RK	PN		00				0		6	Pacific/Pitcairn	2016-07-27
4030691	Water Valley	Water Valley	Water Valley	-25.06667	-130.1	T	VAL	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030692	Ted Side	Ted Side		-25.06667	-130.1	L	AREA	PN		00				0		92	Pacific/Pitcairn	2010-08-13
4030693	Tahoutoumah	Tahoutoumah	Tahoutoumah,Tahoutumah	-24.08333	-130.08333	T	HDLD	PN	PN	00				0		-9999	Pacific/Pitcairn	2012-01-18
4030694	South Point	South Point		-24.42121	-128.29714	T	PT	PN		00				0		28	Pacific/Pitcairn	2016-07-27
4030695	Sandy Islands	Sandy Islands	Sandy Islands,Sandy Islets	-23.91828	-130.73817	T	ISLS	PN	PN	00				0		10	Pacific/Pitcairn	2011-03-16
4030696	Saint Pauls Valley	Saint Pauls Valley		-25.07279	-130.09231	T	VAL	PN		00				0		90	Pacific/Pitcairn	2016-07-27
4030697	Saint Pauls Point	Saint Pauls Point	Saint Paul Point,Saint Paul's,Saint Pauls Point,Saint Paul’s	-25.07536	-130.08796	T	PT	PN	PN	00				0		4	Pacific/Pitcairn	2016-07-27
4030698	The Rope	The Rope		-25.07455	-130.09517	T	CLF	PN		00				0		102	Pacific/Pitcairn	2016-07-27
4030699	Pitcairn, Henderson, Ducie and Oeno Islands	Pitcairn, Henderson, Ducie and Oeno Islands	'Otumotu Pitikeni,Enez Pitcairn,Iles Pitcairn,Illes Pitcairn,Islas Pitcairn,Kepulauan Pitcairn,Orileede Pikarini,Orílẹ́ède Pikarini,Ostrovi Pitkern,Pikairni,Pitakarin,Pitcairn,Pitcairn Islands,Pitcairn uharteak,Pitcairn  Henderson  Ducie and Oeno Islands,Pitcairn-szigetek,Pitcairneilanden,Pitcairnove ostrovy,Pitcairnovy ostrovy,Pitikarini,Pitikeeni,Pitikerni,Pitikeyirini,Pitikêrni,Pitkairn,Pitkairn ƒudomekpo nutome,Pitkairni,Pitkajrn,Pitkarn,Pitkarna Insulo,Pitkehrn,Pitkern,Pitkerna,Pitkernas,Pitkērna,Quan gao Pitcairn,Quần đảo Pitcairn,i-Pitcairn Islands,jzr bytkyrn,pi te kai en qun dao,piskeeonseom,pitaka'irin,pitaka'irna,pitakairna,pitakeyarna,pitkern,pitkeyrn,pitokean dao,pytqrn,Îles Pitcairn,ʻOtumotu Pitikeni,Πίτκερν,Острови Піткерн,Питкайрн,Питкарн,Питкерн,Питкэрн,פיטקרן,جزایر پیت‌کرن,جزر بيتكيرن,پٹکائرن جزائر,पिटकाइर्न,पिटकैर्न,পিটকেয়ার্ন,પીટકૈર્ન,ପିଟକାଇରିନ୍,பிட்கெய்ர்ன்,పిట్కెర్న్,ಪಿಟ್‌ಕೈರ್ನ್,പിറ്റ്കെയ്ന്‍,පිට්කෙය්න් දූපත්,พิตแคร์น,ፒትካኢርን,ピトケアン島,皮特凯恩群岛,핏케언섬	-25.06667	-130.1	A	PCLD	PN		00				46		92	Pacific/Pitcairn	2014-10-01
4030700	Pitcairn Island	Pitcairn Island	Iles Pitcairn,Illa de Pitcairn,Illes Pitcairn,Insulele Pitcairn,Islas Pitcairn,Isole Pitcairn,Kepulauan Pitcairn,Nesoi Pitkairn,Occas Island,Ostrova Pitkehrn,Ostrva Pitkern,Pitcairn,Pitcairn Adasi,Pitcairn Adası,Pitcairn Island,Pitcairn Islands,Pitcairn's Island,Pitcairn-szigetek,Pitcairneilanden,Pitcairninseln,Pitcairnoearna,Pitcairnove ostrovy,Pitcairnovy ostrovy,Pitcairnoyene,Pitcairnski otoki,Pitcairnöarna,Pitcairnøyene,Pitcairn’s Island,Pitkarna Insulo,Pitkehrn,Pitkern,Pitkern Ailen,Pitkerno salos,Pitkerns'ki ostrovi,Ynysow Pitcairn,jzr bytkyrn,pi te kai en qun dao,piskeeon jedo,pitokean zhu dao,pytqrn,Îles Pitcairn,Νήσοι Πίτκαιρν,Острва Питкерн,Острова Питкэрн,Питкэрн,Піткернські острови,פיטקרן,جزر بيتكيرن,หมู่เกาะพิตแคร์น,ピトケアン諸島,皮特凯恩群岛,핏케언 제도	-25.07075	-130.10834	T	ISL	PN	PN	00				0		224	Pacific/Pitcairn	2013-02-03
4030701	Parlver Valley Ridge	Parlver Valley Ridge	Parlver Valley Ridge	-25.06667	-130.1	T	RDGE	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030702	Outer Valley	Outer Valley		-25.06667	-130.1	T	VAL	PN		00				0		92	Pacific/Pitcairn	1993-12-22
4030703	Oeno Atoll	Oeno Atoll	Oeno,Oeno Atoll,Oeno Island,oeno dao,オエノ島	-23.92896	-130.74442	T	ATOL	PN	PN	00				0		9	Pacific/Pitcairn	2011-03-16
4030704	North East Point	North East Point		-24.33169	-128.30186	T	PT	PN		00				0		16	Pacific/Pitcairn	2016-07-27
4030705	North East Point	North East Point		-23.91667	-130.73333	T	PT	PN		00				0		-9999	Pacific/Pitcairn	1993-12-22
4030706	Middle Hill	Middle Hill		-25.06861	-130.11284	T	HLL	PN		00				0		317	Pacific/Pitcairn	2016-07-27
4030707	McCoys Valley	McCoys Valley		-25.07061	-130.09714	T	VAL	PN		00				0		147	Pacific/Pitcairn	2016-07-27
4030708	Manelee	Manelee		-25.08333	-130.08333	H	COVE	PN		00				0		-9999	Pacific/Pitcairn	1993-12-22
4030709	Lookout Point	Lookout Point		-25.06667	-130.1	T	PT	PN		00				0		92	Pacific/Pitcairn	1993-12-22
4030710	Long Ridge	Long Ridge	Long Ridge	-25.06667	-130.1	T	RDGE	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030711	Hollyander	Hollyander		-25.07095	-130.10323	T	UPLD	PN		00				0		211	Pacific/Pitcairn	2016-07-27
4030712	The Hollow	The Hollow	The Hollow	-25.06667	-130.1	T	VAL	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030713	Henderson Island	Henderson Island	Elizabeth Island,Henderson,Henderson Island,Henderson-sziget,Ile Henderson,Ilha Henderson,Isla Henderson,San Juan Bautista,hendason dao,Île Henderson,ヘンダーソン島	-24.37368	-128.32615	T	ISL	PN	PN	00				0		27	Pacific/Pitcairn	2017-05-17
4030714	Gudgeen	Gudgeen		-25.06667	-130.1	H	COVE	PN		00				0		92	Pacific/Pitcairn	1993-12-22
4030715	Gense Valley	Gense Valley	Gense Valley	-25.06667	-130.1	T	VAL	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030716	Garnets Ridge	Garnets Ridge	Garnets Ridge	-25.06667	-130.1	T	RDGE	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030717	Ducie Atoll	Ducie Atoll	Ducie,Ducie Atoll,Ducie Island	-24.67406	-124.77733	T	ATOL	PN	PN	00				0		8	Pacific/Pitcairn	2011-03-16
4030718	Deep Valley	Deep Valley	Deep Valley	-25.06667	-130.1	T	VAL	PN		00				0		92	Pacific/Pitcairn	2018-12-05
4030719	Christians Cave	Christians Cave		-25.06667	-130.1	S	CAVE	PN		00				0		92	Pacific/Pitcairn	1993-12-22
4030720	Point Christian	Point Christian		-25.06871	-130.12134	T	PT	PN		00				0		26	Pacific/Pitcairn	2016-07-27
4030721	Bounty Bay	Bounty Bay		-25.06717	-130.09435	H	BAY	PN		00				0		12	Pacific/Pitcairn	2016-07-27
4030722	Bens Place	Bens Place	Bens Place	-25.06667	-130.08333	T	ATOL	PN		00				0		-9999	Pacific/Pitcairn	2018-12-05
4030723	Adamstown	Adamstown	Adams Town,Adamstaun,Adamstaunas,Adamstauno,Adamstaŭno,Adamstown,Antamstaoun,adamusutaun,admztawn,admztwn  pytkrn ayslnd,aedeomseutaun,atamstavun,ya dang si dui,Άνταμσταουν,Адамстаун,Адамстаўн,אדמסטאון,آدمزتاون,ادمزتون، پیتکرن ایسلند,ایڈمز ٹاؤن، جزائر پٹکیرن,ॲडम्सटाउन,ஆடம்ஸ்டவுன்,แอดัมส์ทาวน์,ადამსტაუნი,アダムスタウン,亚当斯敦,애덤스타운	-25.06597	-130.10147	P	PPLC	PN						46		67	Pacific/Pitcairn	2010-07-21
4030724	Adams Rock	Adams Rock		-25.06904	-130.09055	T	RK	PN		00				0		4	Pacific/Pitcairn	2016-07-27
6957439	Gudgeon Harbor	Gudgeon Harbor		-25.0725	-130.1155	H	HBR	PN						0		296	Pacific/Pitcairn	2009-11-23
6957448	John Mills Harbor	John Mills Harbor		-25.07044	-130.11984	H	HBR	PN						0		-9999	Pacific/Pitcairn	2009-11-23
6957449	Western Harbour	Western Harbour		-25.06181	-130.11864	H	HBR	PN						0		46	Pacific/Pitcairn	2009-11-23
8063825	Kundur marqa	Kundur marqa		-25.065	-130.10357	P	PPL	PN						0		54	Pacific/Pitcairn	2011-12-07
8529197	Awahou Point	Awahou Point		-24.33865	-128.3416	T	PT	PN		00				0		29	Pacific/Pitcairn	2016-07-27
10172853	Pitcairn Islands Marine Reserve	Pitcairn Islands Marine Reserve		-25.24966	-130.07813	L	RESN	PN						0		-9999	Pacific/Pitcairn	2015-03-18
