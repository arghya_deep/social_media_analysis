#!/usr/bin/env python
# coding: utf-8

# ## Libraries

# In[1]:


from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import pandas as pd
import os
import re
import matplotlib.pyplot as plt
import time
import tweepy
import csv
import spacy
import json
import datetime
from collections import Counter
from geopy.geocoders import Nominatim
import os


# In[2]:


pd.set_option("display.max_colwidth", 20000)


# ### Twitter keys

# In[3]:


consumer_key = 'kdcOWlnYEx10Oqi4VcKM6Gjam'
consumer_secret = 'sJVTx602Q0kgIiDIqVd78eeIFuUpYFLOuisfLLm1ZY2JT6D5pU'
access_token = '794180330462687232-kDBYdgJ19YppIhzSuoxDWwEUhgaFLkW'
access_token_secret = '9uBc1ugcO3DB5bWZVUzLgR1Kel3RxuohXVy2w1p2u6w3q'


# ### Creating Api Objects

# In[4]:


auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)
user = api.me()
geolocator = Nominatim(user_agent="dataval", timeout=200)
"""
Location = geolocator.geocode(l)
latLong = str(Location.latitude)+","+str(Location.longitude)
tweets['location'] = latLong
"""


# #### Identified handles for bjp hosted pages

# In[5]:


bjp_pg_handles = ['@ModiFor_2019','@ModiforNewIndia','@TigerNaMo','@iSupportNamo','@NaMoFanGaurav','@parthusoni'
                  ,'@Modiarmy' ,'@Lakshya2019','@ModiBharosa','@NaMoleague','@BJP4India','@NarendraModi1FC']
cong_pg_handles = ['@INCSandesh','@WithCongress','@WithPGV','@PriyaankaGandhi','@Fekunama','@FekuExpress'
                   ,'@DrTharoorFan','@manjumassey','@Kundra_Vishal','@INCIndia','@_ManmohanSingh']


# In[6]:


# aus_tags = ['boat people']
aus_tags = ['boat people', "#refugees", "#asylumseekers", "#withrefugees", "asylum"]


# In[7]:


screen_nm_arr = ['@BJYM','@ModiforNewIndia','@PM_Narendermodi','@NaMoleague','@BJPPath','@NarendraModi1FC',
                 '@Modiarmy','@TigerNaMo','@iSupportNamo','@ModiBharosa','@parthusoni']

bjp_pg_2 = ['@ModiFor_2019','@ModiforNewIndia','@TigerNaMo','@iSupportNamo','@NaMoFanGaurav','@parthusoni'
                  ,'@Modiarmy' ,'@Lakshya2019','@ModiBharosa','@NaMoleague','@BJP4India','@NarendraModi1FC']


bjp_leader2 = ['@narendramodi','@arunjaitley','@ptshrikant','@MahikaInfra','@amitrajwant','@AakankshaCharu',
              '@meeraremi11','@SumanSh58123278']
#'bjp4india'
bjp_political_leaders = ['@amitmalviya','@sambitswaraj','@AmitShah','@rajnathsingh','@narendramodi','@arunjaitley',
                         '@SudhanshuTrived','@smritiirani','@sureshpprabhu','@nitin_gadkari','@PiyushGoyal']

congress_pages1 = ['@INCSandesh','@WithCongress','@WithPGV','@PriyaankaGandhi',
                  '@Fekunama','@FekuExpress','@DrTharoorFan','@manjumassey','@Kundra_Vishal','@INCIndia','@_ManmohanSingh']
cong_political_leaders1 = ['@rssurjewala','@RahulGandhi','@divyaspandana','@SachinPilot',
                          '@ShashiTharoor','@PChidambaram_IN','@NayakRagini','@priyankac19','@Sharmistha_GK','@sushmitadevmp']

#@Anti_ModiHandle
cong_political_leaders2 = ['@Yogita_singh11','@Kundra_Vishal','@manjumassey',
                           '@iammqh','@rssurjewala','@RahulGandhi','@RuchiraC','@Pchidambaram_IN']
congress_pages2 = ['mdbaid','@INCTharoorian','@WithCongress','@INCIndia','@IYC','@FekuBuster','@KingdomOfFeku'
                      ,'@OmanojKumar','@DesiPoliticks','@FekuLeaks','@WithPGV','@Gadhbandhan']

random_tags = ['BijuBabuTrailer','Reliance Communications', '#K13Teaser','#RangDeKesari',
                  '#BigbossFameMumtaz','Meditation','#Mindtree','Goldman Sachs','#StocksInNews','#RedmiGo']

random_tags2 = ['Analytics', '#Android ', '#App ', '#Apple','#WeRun4Cricket','#Cricket','#bollywoodactress',
               '#fashion','#style','#instagood']
randomtags3 = ['#tuesdaythoughts','#GangsofMadras','#airconditioning','#ShadesOfNature','#StockMarketNews',
                  'Cyclone Idai','#USDINR']
randomtags4 = ['#IndianFootball','#HeroISL',"#AapkiNayiDuniya"]
randomtags5= ['#Tech','#Startups','#Food','Jet Airways','#Thalapathy63WithSunTV','#IncredibleIndia','#beaches']
randomtags6 = ['#GetEarlySalary','#kesarionpaytm','shades of nature','#FindBetterFaster','#DadduComingSoon'
                  '#SnapTheColour','#quote','#WorkLifeBalance','#WorkLifeBalance','#RedmiGo','#IPLSchedule','#IPL2019']
randomtags7 = ['#QuPlayPinchKaranJohar','Goldman Sachs','Meditation','Ericsson','#RedmiGo','Rs 453','Utrecht',
                  '#PVRNoDardChallenge','#PVRNoDardChallenge','#TuesdayMorning','#Holi2K19','Techies','Techies','PVR Movie Vouchers']
randomtags8 =['#WednesdayWisdom','#WorldSparrowDay','#FACup','#KalankTeaser','#Boeing737Max','#PewdiepieInsultsIndia',
                     '#TSeries','MS Excel','Will Smith','#WomenOfKalank','#MenOfKalank','#CaptainMarvel',
             '#AmbaniWedding','#ApnaTimeAyega']
randomtags9 =['#ShlokaMehta','#InternationalDayOfHappiness','Zenfone Max Pro M1','#AllNewCivic','#GodsAtPlay',
             '#SaturdayMotivation','#Riverdale','Yes Bank','#XMenDarkPhoenix','#Saaho']
randomtags10 =['#ShadesOfSaahoChapter2','#CleanWaterandSanitation','#GOAT','#FantasyCricket','#11Wickets','#EastBengalFC']
randomtags11 =['#Oscars','#AcademyAwards','#LifeAtTitan','#377अबNormal','#PeriodEndOfSentence','Liverpool','Freddie Mercury',
                  'Rami Malek','#FridayMotivation','#GalaxyS10','God of Cricket']


# In[8]:


handles = 'tanya_plibersek'


# #### Tweets extraction function

# In[27]:


def get_tweets(handle_name, place):
#     stuff = api.user_timeline(screen_name = handles, count=200, include_rts = True,  tweet_mode="extended")
#     search = tweepy.Cursor(stuff).items(1000)
    search = tweepy.Cursor(api.user_timeline, id=handles).items(10)

    #     api.user_timeline, q = handle_name, geocode=place, count=200,result_type="recent", tweet_mode='extended'
    return search


# #### File path

# In[24]:


#file_path = "/home/saurabh/Downloads/data/data_Politics/Pro_bjp1.json"
os_path = os.getcwd()
file_path = ""
def get_file_name():
    currentDT = datetime.datetime.now()
    date_time = currentDT.strftime("%Y-%m-%d %H:%M:%S").replace(" ", "_")
#     path = os_path + "/data/Pro_Cong1/" + date_time +".json"
    path = os_path + "/data/" + handles +".json"
    global file_path
    file_path = path


# In[13]:


cities = ["Sydney,  Australia", "Melbourne ,  Australia", "Brisbane,  Australia", "Perth ,  Australia", 
          "Adelaide,  Australia", "Gold Coast–Tweed Heads,  Australia", "Newcastle–Maitland,  Australia", 
          "Canberra–Queanbeyan ,  Australia", "Sunshine Coast,  Australia", "Wollongong,  Australia", 
          "Geelong ,  Australia", "Hobart,  Australia", "Townsville,  Australia", "Cairns,  Australia", 
          "Darwin,  Australia", "Toowoomba ,  Australia", "Ballarat,  Australia", "Bendigo ,  Australia",
          "Albury–Wodonga,  Australia", "Launceston,  Australia"]
aus_lat_long_lst = []
for city in cities:
    Location = geolocator.geocode(city)
    latLong = str(Location.latitude)+","+str(Location.longitude)+",100km"
    aus_lat_long_lst.append(latLong)
print(aus_lat_long_lst)


# #### Getting data and Writing to disk Functions

# In[29]:


def get_tweets_as_list_json(search_result):
    tweets_list = []
    for tweet in search_result:
        json_tweet = tweet._json
        tweets_list.append(json_tweet)
    return tweets_list

def store_tweets(handles):
    get_file_name()
    global file_path
    tweets = {}
    tweets['list_of_tweets'] = []
    i=1
#     cities = ["sydney"]
    for screen_nm in handles:
        print(i)
        i += 1
        aus_lat_long_lst = ['jamtara']
        for each_city in aus_lat_long_lst:
            tweets_searched = get_tweets(screen_nm, each_city)
            tweets['list_of_tweets'].extend(get_tweets_as_list_json(tweets_searched))
#     print(tweets['list_of_tweets'])
    
    with open(file_path,'w+') as file:
        json.dump(tweets,file)
    print('End')


# In[30]:


store_tweets(handles)


# ### Converting File to pandas DataFrame

# #### Preliminary data cleaning and key extraction from Json functions

# In[51]:


def clean_tweet(tweet_text):
    text = ""
    for wrd in tweet_text.split():
        if not wrd.startswith("http"):
            text += wrd+" "
    return text.strip()

def extract_tweet_attributes(search):
#     print(search)
    tweets_attributes_list = []
    print("start")
    try:
        for item in search:
            print(item)
            tweets = {}
            json_obj = item
            if('retweeted_status' in json_obj):
                tweets['tweet_text'] = json_obj['retweeted_status']['full_text'].replace('\n', ' ').replace('\r', '')
                tweet_text = json_obj['retweeted_status']['full_text'].replace('\n', ' ').replace('\r', '')
            else:
                tweets['tweet_text'] = json_obj['full_text'].replace('\n', ' ').replace('\r', '') if 'full_text' in json_obj else ""
                tweet_text = json_obj['full_text'].replace('\n', ' ').replace('\r', '') if 'full_text' in json_obj else ""    
                
            tweets['clean_tweet_text'] = clean_tweet(tweet_text)
            
            tweets['retweet_count'] = json_obj['retweet_count']
            
            print('retweet count {}'.format(json_obj['retweet_count']))
            
            if 'favourites_count' in json_obj:
                tweets['favourites_count'] = item['user']['favourites_count']    
            else:
                tweets['favourites_count'] = ""
            
            print('1')
                
            if 'followers_count' in json_obj:
                tweets['followers_count'] = item['user']['favourites_count']
            else:
                tweets['followers_count'] = ""
            print('2')
            tweets['language'] = item['user']['lang']
            
            print('Completed favourites,followers,language')
    
            try:
                tweets['location'] = json_obj['user']['location']
               
            except  Exception as e:
                tweets['location'] = "Not Known"
            
            print('3')
            
            if 'retweeted_status' in json_obj:
                original_user = json_obj['retweeted_status']
                user_info = original_user['user']     
                tweets['original_tweeter'] = user_info['screen_name'] 
                
            else:
                tweets['original_tweeter'] = ""

            print('4')
            hashtags_array = []
            if 'entities' in json_obj and 'hashtags' in json_obj['entities']:
                for e in json_obj['entities']['hashtags']:
                    hashtags_array.append(e['text'])
            tweets['hashtags'] = hashtags_array
            
            user_mentions_array = []
            
            print('5')
            if 'entities' in json_obj and 'user_mentions' in json_obj['entities']:
                for e in json_obj['entities']['user_mentions']:
                    user_mentions_array.append(e['screen_name'])
                    #print(e['text'])
            tweets['user_mentions'] =  user_mentions_array
           
            tweets['created_at'] = json_obj['created_at']
            tweets['tweet_id'] = 'twitter.com/anyuser/status/'+json_obj['id_str']
            
            print('6')
            
#             print('Completed location,retweeted_status,hashtags,user_mentions,created_at')

            tweets_attributes_list.append(tweets)
            print(tweets)
            print("args")
    except Exception as e:
        print("Exception Occured: " + e)
    
    print('finally out of loop ==><==')
    
    return tweets_attributes_list


# In[52]:


#bjp_path = path+"/data/Pro_bjp1.json"
#print(bjp_path)
def getDictionary(file_path):
    list_dictionaries = []
    with open(file_path,'r') as f:
        bjpData = json.load(f)
        list_dictionaries.extend(bjpData.get('list_of_tweets'))
#         print(list_dictionaries)
    tweets = extract_tweet_attributes(list_dictionaries)
    print(tweets)
    return tweets


# #### Data Frame creation

# In[53]:


print(file_path)
data = pd.DataFrame(getDictionary(file_path))


# In[39]:


data.head(n=1000)


# In[ ]:





# In[ ]:




