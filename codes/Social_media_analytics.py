from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import pandas as pd
import re
import matplotlib.pyplot as plt
import time
import tweepy
import csv
import spacy
from collections import Counter
import warnings

warnings.filterwarnings('ignore')
consumer_key = 'kdcOWlnYEx10Oqi4VcKM6Gjam'
consumer_secret = 'sJVTx602Q0kgIiDIqVd78eeIFuUpYFLOuisfLLm1ZY2JT6D5pU'
access_token = '794180330462687232-kDBYdgJ19YppIhzSuoxDWwEUhgaFLkW'
access_token_secret = '9uBc1ugcO3DB5bWZVUzLgR1Kel3RxuohXVy2w1p2u6w3q'
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)
user = api.me()
tag = 'INDvsAUS'
tweet_csv = 'data/' + tag + '.csv'
fieldnames = ("code", "lang")


def limit_handled(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            print("waiting")
            time.sleep(15 * 60)


def get_tweets(searchWord):
    search = tweepy.Cursor(api.search, q=searchWord,
                           result_type="mixed", tweet_mode='extended').items(100)
    return search


def get_tweet_list(search):
    tweets_infos = []
    try:
        for item in search:
            tweets = {}
            json_obj = item._json
            tweets['tweet_text'] = json_obj['full_text'].replace('\n', ' ').replace('\r',
                                                                                    '') if 'full_text' in json_obj else ""
            tweets['retweet_count'] = json_obj['retweet_count']
            tweets['favourites_count'] = item.user.favourites_count
            tweets['followers_count'] = item.user.followers_count
            hashtags_array = []
            if 'entities' in json_obj and 'hashtags' in json_obj['entities']:
                for e in json_obj['entities']['hashtags']:
                    hashtags_array.append(e['text'])
            tweets['hashtags'] = hashtags_array

            if len(tweets) > 0:
                tweets_infos.append(tweets)
    except Exception as e:
        print("Exception Occured: " + e)
    return tweets_infos


def create_csv(search):
    tweets_infos = get_tweet_list(search)
    with open(tweet_csv, 'w', encoding="utf-8", newline='') as f:
        # Assuming that all dictionaries in the list have the same keys.
        if len(tweets_infos) > 0:
            headers = sorted([k for k, v in tweets_infos[0].items()])
            csv_data = [headers]

            for d in tweets_infos:
                csv_data.append([d[h] for h in headers])

            writer = csv.writer(f)
            writer.writerows(csv_data)
    f.close()


def get_df(tweet_csv):
    tweets_data = pd.read_csv(tweet_csv, encoding='utf-8')
    return tweets_data


def get_hashtags(tweets_data):
    hashtags = tweets_data['hashtags'].tolist()
    text = ""
    hastags_lst = []
    for lst in hashtags:
        if len(lst) != 0:
            for wrd in lst.split(","):
                wrd = wrd.replace("'", "")
                wrd = wrd.replace("[", "")
                wrd = wrd.replace("]", "")
                if wrd != "":
                    text += " " + wrd.strip()
                    hastags_lst.append(wrd.strip())
    hashtags_df = pd.DataFrame(data=hastags_lst, columns=['hashtags'])
    hashtags_df.head()
    wordcloud = WordCloud(background_color="white").generate(text)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()
    unique_hashtags_df = hashtags_df['hashtags'].value_counts().rename_axis('hashtags').reset_index(name='counts')
    unique_hashtags_df.head(15)
    print("hastags : \n")
    return unique_hashtags_df


def get_word_clouds(tweets_data):
    nlp = spacy.load('en')

    nlp.vocab["RT"].is_stop = True
    nlp.vocab["ji"].is_stop = True

    regex = re.compile('^[https:// # @]')
    tweets_data['text'] = tweets_data['tweet_text'].apply(
        lambda x: ' '.join([word for word in x.split() if not regex.match(word)]))
    tweets_data = tweets_data.sort_values(['retweet_count', 'favourites_count', 'followers_count'],
                                          ascending=False).head(10)

    tweets_text_df = tweets_data['text'].tolist()

    entire_text = ""
    for tweet in tweets_text_df:
        entire_text += tweet + " "
    doc = nlp(entire_text)
    words = [token.text for token in doc if token.is_stop != True and token.is_punct != True and token.pos_ != "CONJ"
             and token.pos_ != "PRON" and token.pos_ != "DET"]

    new_text = ""
    for tweet in words:
        new_text += tweet + " "
    wordcloud = WordCloud(background_color="white").generate(new_text)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()

    word_freq = Counter(words)
    common_words = word_freq.most_common(12)
    keywords_df = pd.DataFrame(common_words, columns=['keyword', 'keyword_count'])
    print("keywords_df : \n")
    print(keywords_df)
    return keywords_df


def get_htag(data_tweets):
    hastags_lst = []
    text = ""
    for row in data_tweets['hashtags']:
        for wrd in row:
            if wrd != "":
                text += " " + wrd.strip()
                hastags_lst.append(wrd.strip())
    hashtags_df = pd.DataFrame(data=hastags_lst, columns=['hashtags'])
    hashtags_df.head()
    wordcloud = WordCloud(background_color="white").generate(text)

    # Display the generated image:
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()
    unique_hashtags_df = hashtags_df['hashtags'].value_counts().rename_axis('hashtags').reset_index(name='counts')
    print("in get_htag")
    print(unique_hashtags_df)
    unique_hashtags_df.head(15)
    return unique_hashtags_df


def rec_call(tag):
    tweets_searched = get_tweets(tag)
    data_tweets = pd.DataFrame(get_tweet_list(tweets_searched))
    print("data csv in rec")
    print(data_tweets.head())
    hashtags_df = get_htag(data_tweets)  # get_hashtags(data_tweets)
    keywords_df = get_word_clouds(data_tweets)
    trends_df = pd.concat([hashtags_df, keywords_df], axis=1)
    print("n set of trend")
    print(trends_df.head())
    return trends_df


def main():
    tweets_searched = get_tweets("#" + tag)
    create_csv(tweets_searched)
    tweets_data = get_df(tweet_csv)
    hashtags_df = get_hashtags(tweets_data)
    keywords_df = get_word_clouds(tweets_data)
    trends_df = pd.concat([hashtags_df, keywords_df], axis=1)
    print("first set of trend")
    print(trends_df.size)
    i = 0
    j = 0
    for each_hashtag in hashtags_df.hashtags:
        i += 1
        each_tag_df = rec_call("#" + each_hashtag)
        print("Size of each_tag_df ")
        print(each_tag_df.size)
        print("Size of trends_df ")
        print(trends_df.size)
        trends_df.append(each_tag_df, ignore_index=True)
        print("i = \n")
        print(i)
        print(trends_df)

    for each_keyword in keywords_df.keyword:
        j += 1
        each_tag_df = rec_call(each_keyword)
        print("Size of each_tag_df ")
        print(each_tag_df.size)
        print("Size of trends_df ")
        print(trends_df.size)
        trends_df.append(each_tag_df, ignore_index=True)
        print("j = \n")
        print(j)
        print(trends_df)

    return trends_df


trends = main()
print(trends)
print("size = ")
print(trends.size)
print("args")
