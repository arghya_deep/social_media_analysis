import csv
import random
import json, time, re, nltk, threading
import pandas as pd
from datetime import datetime as dt
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from http.client import IncompleteRead
from tweepy import Stream
from elasticsearch import Elasticsearch
from senti_classifier import senti_classifier
from urllib3.exceptions import ProtocolError

# nltk.download('punkt')
# nltk.download('wordnet')


tweet_csv = 'data/tweet_data.csv'



class TweetStreamListener(StreamListener):
    def __init__(self, count=0):
        self.counter = 0
        self.limit = count
        
    def on_data(self, data):
        temp_dict1, temp_dict2 = {}, {}
        if self.limit is not 0:
            self.counter +=1
            if self.counter > self.limit:
                return False
        dict_data = json.loads(data)
        
        #print(data)
        try:
            text = dict_data["text"]
        except KeyError:
            return True
        hashtags = []
        for hashtag in dict_data["entities"]["hashtags"]:
            tempdict = {"hashtag": hashtag["text"]}
            hashtags.append(tempdict)
            
        urltags = []
        for url in dict_data["entities"]["urls"]:
            tempdict = {"url": url["url"]}
            urltags.append(tempdict)
            #print(hashtag["text"])
        temp_text = re.sub(r'RT(\s@\w+:)|http\S+|[^A-Za-z\s]','',text)
        pos_value,neg_value = senti_classifier.polarity_scores([temp_text])
        temp_dict1['senti-classifier-positive-score'] = pos_value
        temp_dict1['senti-classifier-negative-score'] = neg_value
        temp_dict2['sentiment-classifier-statement'] = "Positive" if((pos_value-neg_value) > 0) else "Neutral" if((pos_value-neg_value)==0) else "Negative"
        dict_data['sentiment'] = temp_dict2['sentiment-classifier-statement']
        
        coordinates=[]
        randX=random.random()*8
        randY=random.random()*8
        boundingX=22.3
        boundingY=72.5
        #if(dict_data["coordinates"] is None):
        coordinates=[str(boundingX+randX)+","+str(boundingY+randY)]
        #print(coordnates)
        #else:
        #print(dict_data["coordinates"])
        print(text)
        print(dict_data['text'])
        print(dict_data['created_at'])
        requiredJSONbody={"screen_name": dict_data["user"]["screen_name"],
                       "user_name": dict_data["user"]["name"],
                       "location": dict_data["user"]["location"],
                       "followers_count": dict_data["user"]["followers_count"],                       
                       #"geo": dict_data["geo"],
                       "coordinates": coordinates, 
                       #"coordinates":dict_data["coordinates"],
                       #"place": dict_data["place"],
                       "user_mentions": dict_data["entities"]["user_mentions"],
                       "urls":urltags,
                       "retweeted": dict_data["retweeted"],
                       "created_at": dt.strptime(dict_data['created_at'], "%a %b %d %H:%M:%S %z %Y")\
                       .strftime("%Y-%M-%d %H:%M:%S"),
                       "message": dict_data["text"],
                       "hashtags": hashtags,
                       "favorite_count": dict_data["favorite_count"],
                       "retweet_count": dict_data["retweet_count"],
                       "sentiment": dict_data["sentiment"]}



        with open(tweet_csv, 'r+') as outfile:
            reader = csv.reader(outfile)
            flag = False
            headers = sorted([k for k, v in requiredJSONbody.items()])
            for row in reader:
                if row == headers:
                    flag = True
                break
            if flag:
                csv_data = [headers]

            csv_data.append([requiredJSONbody[a] for a in headers])
            writer = csv.writer(outfile)
            writer.writerows(csv_data)
            # json.dump(requiredJSONbody, outfile, sort_keys = True, indent = 4,
            #    ensure_ascii = False)

        tweets_data = pd.read_csv(tweet_csv, encoding='utf-8')
        unique_users = tweets_data['user_name'].value_counts().rename_axis('user_name').reset_index(name='user_name_counts')

        print("hello args")

        #return
        index = "twit"
        document_type = "tweets"

    def on_error(self, status):
        print("here")
        print(status)

to_track = ["rahul"]
languages = ['en']
bjp_political_leaders = ['@amitmalviya','@sambitswaraj','@AmitShah','@rajnathsingh','@narendramodi','@arunjaitley',
                         '@SudhanshuTrived','@smritiirani','@sureshpprabhu','@nitin_gadkari','@PiyushGoyal']

consumer_key = 'kdcOWlnYEx10Oqi4VcKM6Gjam'
consumer_secret = 'sJVTx602Q0kgIiDIqVd78eeIFuUpYFLOuisfLLm1ZY2JT6D5pU'
access_token = '794180330462687232-kDBYdgJ19YppIhzSuoxDWwEUhgaFLkW'
access_token_secret = '9uBc1ugcO3DB5bWZVUzLgR1Kel3RxuohXVy2w1p2u6w3q'


listener = TweetStreamListener()
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
while True:
    try:
        stream = Stream(auth, listener)
        stream.filter(follow=["95588504", "1368737382", "1447949844", "1346439824", "18839785", "2183816041",
                              "252018855", "130104041", "141208596", "97217966", "1324334436"])        #locations= location)
    except ProtocolError:
        continue
    except IncompleteRead:
        continue
    except KeyboardInterrupt:
        stream.disconnect()
    break
