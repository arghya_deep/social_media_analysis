from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from textblob import TextBlob
import pandas as pd
import re
import os
import time
import tweepy
import csv
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
import plotly
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator


plotly.tools.set_credentials_file(username='join2saurav', api_key='r8gIeW5ie1M7cTaLda4d')

def clean_tweet(tweet):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\\w+:\\/\\S+)", " ", tweet).split())


def get_tweet_sentiment(tweet):
    analysis = TextBlob(clean_tweet(tweet))
    if analysis.sentiment.polarity > 0:
        return 'positive'
    elif analysis.sentiment.polarity == 0:
        return 'neutral'
    else:
        return 'negative'


consumer_key = 'kdcOWlnYEx10Oqi4VcKM6Gjam'
consumer_secret = 'sJVTx602Q0kgIiDIqVd78eeIFuUpYFLOuisfLLm1ZY2JT6D5pU'
access_token = '794180330462687232-kDBYdgJ19YppIhzSuoxDWwEUhgaFLkW'
access_token_secret = '9uBc1ugcO3DB5bWZVUzLgR1Kel3RxuohXVy2w1p2u6w3q'

# OAuth process, using the keys and tokens
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

# creation of the actual interface, using authentication
api = tweepy.API(auth)

user = api.me()
TAG = 'Raga'
HASHTAG = "#"+ TAG

LC_path_json = 'data/language_code.json'
tweet_csv = 'data/'+TAG+'.csv'
print_to_csv = 'data/'+TAG+'.csv'
# os.chdir('..')
jsonfile = open(os.getcwd()+"/"+LC_path_json, 'r')
api = tweepy.API(auth)
fieldnames = ("code","lang")
reader = jsonfile.read()


def limit_handled(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            time.sleep(15 * 60)
            print("args in exception")


search = limit_handled(tweepy.Cursor(api.search, q=HASHTAG,
                                     result_type="mixed", tweet_mode='extended').items(100))
# geolocator = Nominatim(user_agent="dataval", timeout=200)

tweets_infos = []
for item in search:
    tweets = {}
    json_obj = item._json
    tweets['tweet_text'] = json_obj['full_text'].replace('\n', ' ').replace('\r', '') if 'full_text' in json_obj else ""
    tweets['retweet_count'] = json_obj['retweet_count']
    tweets['favourites_count'] = item.user.favourites_count
    tweets['followers_count'] = item.user.followers_count
    hashtags_array = []
    if 'entities' in json_obj and 'hashtags' in json_obj['entities']:
        for e in json_obj['entities']['hashtags']:
            hashtags_array.append(e['text'])
    tweets['hashtags'] = hashtags_array

    if len(tweets) > 0:
        tweets_infos.append(tweets)

with open(print_to_csv, 'w',encoding="utf-8", newline='') as f:
    # Assuming that all dictionaries in the list have the same keys.
    if len(tweets_infos) > 0:
        headers = sorted([k for k, v in tweets_infos[0].items()])
        csv_data = [headers]

        for d in tweets_infos:
            csv_data.append([d[h] for h in headers])

        writer = csv.writer(f)
        writer.writerows(csv_data)
f.close()
tweets_data = pd.read_csv(tweet_csv)
tweets_data.head()
stop = stopwords.words('english', 'hindi')
regex = re.compile('^[https:// # @]')
tweets_data['text'] = tweets_data['tweet_text'].apply(lambda x: ' '.join([word for word in x.split() if word not in
                                                                          stop and not regex.match(word)]))
tweets_data = tweets_data.sort_values(['retweet_count', 'favourites_count', 'followers_count'], ascending=False).head(10)

# tweet = tweets_data.groupby(['text']).
# tweets_data['ratio_retweet'] = tweets_data['retweet_count'].apply(lambda x: x/tweets_data.retweet_count.sum())
# tweets_data['ratio_favourites'] = tweets_data['favourites_count'].apply(lambda x: x/tweets_data.favourites_count.sum())
# tweets_data['ratio_followers'] = tweets_data['followers_count'].apply(lambda x: x/tweets_data.followers_count.sum())
# tweets_data['impact'] = tweets_data.apply(lambda row: row.ratio_retweet + row.ratio_favourites + row.ratio_followers, axis=1)

# result = tweets_data.sort_values(['impact'], ascending=False).head(50)
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(tweets_data.text)
print(count_vect.vocabulary_.items())
#