#!/usr/bin/env python
# coding: utf-8

# In[95]:


from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
# from matplotlib.font_manager import FontProperties
from nltk.stem import PorterStemmer
from collections import Counter
import os
import re
import ast
import csv
import time
import spacy
import tweepy
import warnings
# import matplotlib
import pandas as pd
# import matplotlib.pyplot as plt


# In[96]:


warnings.filterwarnings('ignore')


# In[97]:


nlp = spacy.load('en',disable=['parser','ner','textcat'])
porter=PorterStemmer()
h_stop_words = ["मैं" ,"के" ,"मुझको" ,"मेरा" ,"हमारा" ,'अपना' ,'हम' ,'आप' ,'आपका' ,'तुम्हारा' ,'अपने आप'  ,'स्वयं' ,'वह' ,'इसे' ,
          'उसके' ,'खुदको'  ,'कि वह' ,'उसकी' ,'उसका' ,'खुद ही' ,'यह' ,'इसके' ,'उन्होने' ,'अपने' ,'क्या' ,'जो' ,'किसे' ,
          'किसको' ,'कि' ,'ये' ,'हूँ' ,'होता है' ,'रहे' ,'थी' ,'थे' ,'होना' ,'गया' ,'किया जा रहा है' ,'किया है' ,'है' ,'पडा' ,
          'होने' ,'करना' ,'करता है' ,'किया' ,'रही' ,'एक' ,'लेकिन' ,'अगर' ,'या' ,'क्यूंकि' ,'जैसा' ,'जब तक' ,'जबकि' ,'की' ,
          'पर' ,'द्वारा' ,'के लिए' ,'साथ' ,'के बारे में' ,'खिलाफ' ,'बीच' ,'में' ,'के माध्यम से' ,'दौरान' ,'से पहले' ,'के बाद' ,'ऊपर' ,
          'नीचे' ,'को' ,'से' ,'तक' ,'से नीचे' ,'करने में' ,'निकल' ,'बंद' ,'से अधिक' ,'तहत' ,'दुबारा' ,'आगे' ,'फिर' ,'एक बार' ,
          'यहाँ' ,'वहाँ' ,'कब' ,'कहाँ' ,'क्यों' ,'कैसे' ,'सारे' ,'किसी' ,'दोनो' ,'प्रत्येक' ,'ज्यादा' ,'अधिकांश' ,'अन्य' ,'में कुछ' ,
          'ऐसा' ,'में कोई' ,'मात्र' ,'खुद' ,'समान' ,'इसलिए' ,'बहुत' ,'सकता' ,'जायेंगे' ,'जरा' ,'चाहिए' ,'अभी' ,'और' ,'कर दिया' 
          ,'रखें' ,'का' ,'हैं' ,'इस' ,'होता' ,'करने' ,'ने' ,'बनी' ,'तो' ,'ही' ,'हो' ,'इसका' ,'था' ,'हुआ' ,'वाले' ,'बाद' ,'लिए' ,
          'सकते' ,'इसमें' ,'दो' ,'वे' ,'करते' ,'कहा' ,'वर्ग' ,'कई' ,'करें' ,'होती' ,'अपनी' ,'उनके' ,'यदि' ,'हुई' ,'जा' ,'कहते' ,
          'जब' ,'होते' ,'कोई' ,'हुए' ,'व' ,'जैसे' ,'सभी' ,'करता' ,'उनकी' ,'तरह' ,'उस' ,'आदि' ,'इसकी' ,'उनका' ,'इसी' ,'पे' ,
          'तथा' ,'भी' ,'परंतु' ,'इन' ,'कम' ,'दूर' ,'पूरे' ,'गये' ,'तुम' ,'मै' ,'यहां' ,'हुये' ,'कभी' ,'अथवा' ,'गयी' ,'प्रति' ,'जाता' ,
          'इन्हें' ,'गई' ,'अब' ,'जिसमें' ,'लिया' ,'बड़ा' ,'जाती' ,'तब' ,'उसे' ,'जाते' ,'लेकर' ,'बड़े' ,'दूसरे' ,'जाने' ,'बाहर' ,'स्थान' ,
          'उन्हें ' ,'गए' ,'ऐसे' ,'जिससे' ,'समय' ,'दोनों' ,'किए' ,'रहती' ,'इनके' ,'इनका' ,'इनकी' ,'सकती' ,'आज' ,'कल' ,'जिन्हें' ,
          'जिन्हों' ,'तिन्हें' ,'तिन्हों' ,'किन्हों' ,'किन्हें' ,'इत्यादि' ,'इन्हों' ,'उन्हों' ,'बिलकुल' ,'निहायत' ,'इन्हीं' ,'उन्हीं' ,'जितना' ,'दूसरा' ,
          'कितना' ,'साबुत' ,'वग़ैरह' ,'कौनसा' ,'लिये' ,'दिया' ,'जिसे' ,'तिसे' ,'काफ़ी' ,'पहले' ,'बाला' ,'मानो' ,'अंदर' ,'भीतर' ,'पूरा' ,
          'सारा' ,'उनको' ,'वहीं' ,'जहाँ' ,'जीधर' ,'﻿के' ,'एवं' ,'कुछ' ,'कुल' ,'रहा' ,'जिस' ,'जिन','तिस' ,'तिन' ,'कौन' ,'किस' ,
          'संग' ,'यही' ,'बही' ,'उसी' ,'मगर' ,'कर' ,'मे' ,'एस' ,'उन' ,'सो' ,'अत' ,
          'जी', ',' ,':' ,' ' ,'।' ,'-' ,'!' , '?' ,';' ,'rt' ,'\u200d' ,'➡' ,"'s"]
for s in h_stop_words:
    nlp.vocab[s].is_stop = True


# In[98]:


def removeStopWords(twt):
    doc = nlp(twt)

    words = [token.text for token in doc if token.is_stop != True and token.is_punct != True  and 
             token.pos_ != "CONJ" and token.pos_ != "PRON" and token.pos_ != "DET" and 
             (re.match("[\u0900-\u097F]+",token.text) or re.match("[a-z]+",token.text))]
    new_text = ""

    for word in words:
        new_text +=" "+word
        new_text = new_text.strip()
    
    return new_text.strip()


# auth = None
# api = None
# user = None
# flag = True
# 
# def alter_keys(l_flag):
#     consumer_key, consumer_secret, access_token, access_token_secret = None, None, None, None
#     if l_flag:
#         consumer_key = 'JLsL7OzJkkGfppe4mhbNG1ezs'
#         consumer_secret = 'AP7TfIGi9pMVzQx7uvIjZgjSQfWVNhleTjA4L4shbiJhYk0nlF'
#         access_token = '822501368237793281-V2PxECAeKMgxSz3XYLVvnlhwxqE9JJA'
#         access_token_secret = 'H5aXZT9SEvTdqIUXAvA9CRkbNtNmped42evGBOrjGjhui'
#         flag = False
#     else:
#         consumer_key = 'kdcOWlnYEx10Oqi4VcKM6Gjam'
#         consumer_secret = 'sJVTx602Q0kgIiDIqVd78eeIFuUpYFLOuisfLLm1ZY2JT6D5pU'
#         access_token = '794180330462687232-kDBYdgJ19YppIhzSuoxDWwEUhgaFLkW'
#         access_token_secret = '9uBc1ugcO3DB5bWZVUzLgR1Kel3RxuohXVy2w1p2u6w3q'  
#         flag = True
#     return consumer_key, consumer_secret, access_token, access_token_secret
#     
# def get_auth_api(flag):
#     consumer_key, consumer_secret, access_token, access_token_secret = alter_keys(flag)
#     auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
#     auth.set_access_token(access_token, access_token_secret)
#     api = tweepy.API(auth,wait_on_rate_limit=True)
#     user = api.me()

# In[99]:


consumer_key = 'JLsL7OzJkkGfppe4mhbNG1ezs'
consumer_secret = 'AP7TfIGi9pMVzQx7uvIjZgjSQfWVNhleTjA4L4shbiJhYk0nlF'
access_token = '822501368237793281-V2PxECAeKMgxSz3XYLVvnlhwxqE9JJA'
access_token_secret = 'H5aXZT9SEvTdqIUXAvA9CRkbNtNmped42evGBOrjGjhui'
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)
user = api.me()


# In[100]:


tag = "\"boat people\""


# In[101]:


# os.chdir('..')
path = os.getcwd()
file_name = tag.replace(" ", "_")
tweet_csv = path + '/data/'+ file_name +'.csv'
# hindi_font_path = path + '/DataValAnalysis/data/kruti-dev-021.ttf'
hindi_font_path = path + '/data/Akshar_Unicode.ttf'
fieldnames = ("code","lang")
print(hindi_font_path)


# In[102]:


def get_tweets(searchWord):
    search = tweepy.Cursor(api.search, q=searchWord, 
                                     result_type="mixed", tweet_mode='extended').items(100)
    return search


# In[103]:


def get_tweet_list(search):
    tweets_infos = []
    try:
        for item in search:
            tweets = {}
            json_obj = item._json
            if('retweeted_status' in json_obj):
                tweets['tweet_text'] = json_obj['retweeted_status']['full_text'].replace('\n', ' ').replace('\r', '')
                tweet_text = json_obj['retweeted_status']['full_text'].replace('\n', ' ').replace('\r', '')
            else:
                tweets['tweet_text'] = json_obj['full_text'].replace('\n', ' ').replace('\r', '') if 'full_text' in json_obj else ""
                tweet_text = json_obj['full_text'].replace('\n', ' ').replace('\r', '') if 'full_text' in json_obj else ""
            tweets['retweet_count'] = json_obj['retweet_count']
            tweets['favourites_count'] = item.user.favourites_count
            tweets['followers_count'] = item.user.followers_count
            hashtags_array = []
            if 'entities' in json_obj and 'hashtags' in json_obj['entities']:
                for e in json_obj['entities']['hashtags']:
                    hashtags_array.append(e['text'])
            tweets['hashtags'] = hashtags_array

            if len(tweets) > 0:
                tweets_infos.append(tweets)
    except Exception as e:
        print("Exception Occured: " + e)
    return tweets_infos


# In[104]:


def create_csv(search):
    tweets_infos = get_tweet_list(search)
    with open(tweet_csv, 'w',encoding="utf-8", newline='') as f:
        # Assuming that all dictionaries in the list have the same keys.
        if len(tweets_infos) > 0:
            headers = sorted([k for k, v in tweets_infos[0].items()])
            csv_data = [headers]

            for d in tweets_infos:
                csv_data.append([d[h] for h in headers])

            writer = csv.writer(f)
            writer.writerows(csv_data)
    f.close()


# In[105]:


def get_df(tweet_csv):
    tweets_data = pd.read_csv(tweet_csv,encoding='utf-8')
    return tweets_data


# ####  getting all hashtags

# In[106]:


def get_hashtags(tweets_data):
    hashtags = tweets_data['hashtags'].tolist()
    text = ""
    hastags_lst = []
    for lst in hashtags:
        lst = ast.literal_eval(lst)
        if len(lst)!=0:
            for wrd in lst:
                wrd = removeStopWords(wrd).encode('utf-8').decode('utf-8')
                if wrd!="" and (re.match("[\u0900-\u097F]+",wrd) or re.match("[a-z]+",wrd)):
                    text += " "+wrd.strip()
                    hastags_lst.append(wrd.strip())
    hashtags_df = pd.DataFrame(data=hastags_lst,columns=['hashtags'])
    hashtags_df.head()
    # hindi_font = FontProperties(fname = hindi_font_path)
    # wordcloud = WordCloud(background_color="white", font_path=hindi_font_path).generate(text)
    # fontP = FontProperties()
    #
    # Display the generated image:
    # plt.text(-0.8, 0.9, 'family', fontproperties=hindi_font, **alignment)
    # plt.imshow(wordcloud, interpolation='bilinear')
    # plt.axis("off")
    # plt.show()
    unique_hashtags_df = hashtags_df['hashtags'].value_counts().rename_axis('hashtags').reset_index(name='counts')
    return unique_hashtags_df.head(n=15)


# In[107]:


def get_htag(data_tweets):
    hastags_lst =[]
    text = ""
    for row in data_tweets.itertuples():
        for wrd in row.hashtags:
            if wrd!="" and (re.match("[\u0900-\u097F]+",wrd) or re.match("[a-z]+",wrd)):
                text += " "+wrd.strip()
                hastags_lst.append(wrd.strip())
    hashtags_df = pd.DataFrame(data=hastags_lst,columns=['hashtags'])
    hashtags_df.head()
    # wordcloud = WordCloud(background_color="white").generate(text)
    #
    # plt.text(-0.8, 0.9, 'family', fontproperties=hindi_font_path, **alignment)
    # plt.imshow(wordcloud, interpolation='bilinear')
    # plt.axis("off")
    # plt.show()
    unique_hashtags_df = hashtags_df['hashtags'].value_counts().rename_axis('hashtags').reset_index(name='counts')
    return unique_hashtags_df.head(n=15)


# In[108]:


# matplotlib.rc('font', family='Arial')
alignment = {'horizontalalignment': 'center', 'verticalalignment': 'baseline'}


# #### getting wordcloud of tweets keyword

# In[109]:


def get_word_clouds(tweets_data):
    nlp = spacy.load('en')

    nlp.vocab["RT"].is_stop = True
    nlp.vocab["ji"].is_stop = True

    regex = re.compile('^[https:// # @]')
    tweets_data['text'] = tweets_data['tweet_text'].apply(lambda x: ' '.join([word for word in x.split() if not regex.match(word)]))
    tweets_data = tweets_data.sort_values(['retweet_count', 'favourites_count', 'followers_count'], ascending=False).head(10)

    tweets_text_df = tweets_data['text'].tolist()

    entire_text = ""
    for tweet in tweets_text_df:
        entire_text += tweet + " "
    new_text = removeStopWords(entire_text)
    words = new_text.split(" ")
    # wordcloud = WordCloud(background_color="white").generate(new_text)
    #
    # plt.text(-0.8, 0.9, 'family', fontproperties=hindi_font_path, **alignment)
    # plt.imshow(wordcloud, interpolation='bilinear')
    # plt.axis("off")
    # plt.show()
    #
    word_freq = Counter(words)
    common_words = list()
    common_words.append([(each_word[0].encode('utf-8').decode('utf-8'), each_word[1]) for each_word in word_freq.most_common(12) if re.match("[\u0900-\u097F]+",each_word[0]) or re.match("[a-z]+", each_word[0])])
    keywords_df = pd.DataFrame(common_words[0], columns=['keyword', 'keyword_count'])
    return keywords_df.head(n=10)


# nltk.download('stopwords')
# 

# In[110]:


def rec_call(tag):
    tweets_searched = get_tweets(tag)
    data_tweets = pd.DataFrame(get_tweet_list(tweets_searched))
    # print("data csv in rec")
    # print(data_tweets.head())
    # print("after # printing data_tweets.head")
    hashtags_df = get_htag(data_tweets)#get_hashtags(data_tweets)
    keywords_df = get_word_clouds(data_tweets)
    trends_df = pd.concat([hashtags_df, keywords_df], axis=1)
    # print("n set of trend")
    # print(trends_df.head())
    return trends_df
    


# In[111]:


def main():
    tweets_searched = get_tweets("#"+tag)
    create_csv(tweets_searched)
    tweets_data = get_df(tweet_csv)
    hashtags_df = get_hashtags(tweets_data)
    keywords_df = get_word_clouds(tweets_data)
    trends_df = pd.concat([hashtags_df, keywords_df], axis=1)
    # print("first set of trend")
    # print(trends_df.size)
    i = 0
    j = 0
    main_df_htag = pd.DataFrame()
    main_df_kwrds = pd.DataFrame()
    for each_hashtag in hashtags_df.hashtags:
        i+=1
        each_tag_df = rec_call("#"+each_hashtag)
        crnt_df_htag = pd.DataFrame()
        crnt_df_htag['hashtags'] = each_tag_df['hashtags']
        crnt_df_htag['counts'] = each_tag_df['counts']
        main_df_htag['hashtags'] = trends_df['hashtags']
        main_df_htag['counts'] = trends_df['counts']
        main_df_htag = main_df_htag.append(crnt_df_htag, ignore_index=True)
        main_df_htag = main_df_htag.groupby('hashtags').agg({'counts': 'sum'}).reset_index().rename(columns={"counts": 'counts'})
        crnt_df_kwrds = pd.DataFrame()
        crnt_df_kwrds['keyword'] = each_tag_df['keyword']
        crnt_df_kwrds['keyword_count'] = each_tag_df['keyword_count']
        main_df_kwrds['keyword'] = trends_df['hashtags']
        main_df_kwrds['keyword_count'] = trends_df['keyword_count']
        main_df_kwrds = main_df_kwrds.append(crnt_df_kwrds, ignore_index=True)
        main_df_kwrds = main_df_kwrds.groupby('keyword').agg({'keyword_count': 'sum'}).reset_index().rename(
            columns={"keyword_count": 'keyword_count'})
        trends_df = pd.concat([main_df_htag, main_df_kwrds], axis=1, sort=True)
        print("i = " )
        print(i )

    for each_keyword in keywords_df.keyword:
        j+=1
        each_tag_df = rec_call(each_keyword)
        crnt_df_htag = pd.DataFrame()
        crnt_df_htag['hashtags'] = each_tag_df['hashtags']
        crnt_df_htag['counts'] = each_tag_df['counts']
        main_df_htag['hashtags'] = trends_df['hashtags']
        main_df_htag['counts'] = trends_df['counts']
        main_df_htag = main_df_htag.append(crnt_df_htag, ignore_index=True)
        main_df_htag = main_df_htag.groupby('hashtags').agg({'counts': 'sum'}).reset_index().rename(columns={"counts": 'counts'})
        main_df_htag.sort_values(by='counts',ascending=False,inplace=True)
        crnt_df_kwrds = pd.DataFrame()
        crnt_df_kwrds['keyword'] = each_tag_df['keyword']
        crnt_df_kwrds['keyword_count'] = each_tag_df['keyword_count']
        main_df_kwrds['keyword'] = trends_df['hashtags']
        main_df_kwrds['keyword_count'] = trends_df['keyword_count']
        main_df_kwrds = main_df_kwrds.append(crnt_df_kwrds, ignore_index=True)
        main_df_kwrds = main_df_kwrds.groupby('keyword').agg({'keyword_count': 'sum'}).reset_index().rename(
            columns={"keyword_count": 'keyword_count'})
        main_df_kwrds.sort_values(by='keyword_count',ascending=False,inplace=True)
        trends_df = pd.concat([main_df_htag], axis=1, sort=False)
        print("j = ")
        print(j )

    return main_df_htag, main_df_kwrds


# In[113]:


htag_df_trends, kwrds_df_trends = main()



